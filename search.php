<?php
include_once 'sys/inc/start.php';
$doc = new Document();
$doc->title = 'Поиск новостей';

$search_query_sql = array();
$symbols_mark = array();

if(isset($_GET['s'])) {
    $words = urldecode($_GET['s']);
} elseif(isset($_POST['search'])) {
    $words = $_POST['search'];
} else {
    $doc->accessDenied("На задоны слова для поиска");
}

$search_array = preg_split('#\s+#u', $words);

if(count($search_array) > 8) {
    $doc->accessDenied("Нет смысла искать больше 8-и слов");
}

for ($i = 0; $i < count($search_array); $i++) {
    $word = $search_array[$i];
    
    $word = preg_replace('#[^a-zа-я0-9\-]#ui', '', $word);
    
    if(Text::strlen($word) < 2) {
        continue;
    }

    $search_query_sql[$i] = $word . '*';
    
    $searched_mark[] = '#([a-zа-я0-9]*' . preg_quote($word, '#') . '[a-zа-я0-9]*)#ui';
    $symbols_mark[] = '#('.$word.')#ui';
}


$pages = new Pages();
$pages->st = 3;
$pages->items_per_page = 12;
$res = Db::me()->query("SELECT COUNT(*) FROM `news` WHERE MATCH (`title`)
          AGAINST ('".implode(' ', $search_query_sql)."' IN BOOLEAN MODE)");
$pages->posts = $res->fetchColumn(); // количество сообщений


$res = Db::me()->query("SELECT * FROM `news` WHERE MATCH (`title`)
          AGAINST ('".implode(' ', $search_query_sql)."' IN BOOLEAN MODE) ORDER BY `id` DESC LIMIT " . $pages->limit);

$listing = new Listing('news.php');

foreach($res->fetchAll() as $news) {
    $post = $listing->post();

    $is_title = preg_replace($searched_mark,
        '<span style="background-color: #CDDCE8;">\\1</span>', $news['title']);
    $post->title = preg_replace($symbols_mark,
        '<span style="color: #0088EF;">\\1</span>', $is_title);
    $post->url = "/news/" . Text::urlToString($news['id'] . "-" . $news['title']);
    $post->img = "/sys/files/news/original/" . $news['img'];
    $post->text = Text::substr($news['text'], 190);
    $post->views = $news['views'];
    $post->comments = $news['comments'];
    $post->time = $news['time'];
    $post->off_news = $news['off_news'];
}

$listing->display('Поиск не дал результата', 'textView.php');

$zap1 = urlencode($words);
$url = new Url('/search.php?s=' . $zap1);
$url->setPath('/search.php');
$pages->display((string)$url . '&'); // вывод страниц