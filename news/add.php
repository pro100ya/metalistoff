<?php
include_once '../sys/inc/start.php';
$doc = new Document(1);
$doc->title = "Добавить новость";


$form = new Form();
$form->input("title", "Заголовок");

$options = array();
$options[] = array(0, 'Не выбрано');
$options[] = array(1, 'Ongoing');
$options[] = array(2, 'OVA');
$options[] = array(3, 'Full');
$options[] = array(4, 'Movie');
$options[] = array(5, '100+');
$form->select("type", "Тип новости (Главная, Лента)", $options);

$form->input("tags", "Теги (Футбол,Трансферы,Франция)");
$form->file('img', "Постер");
$form->textarea("description", "Описание");
$form->button("Сохранить", "save");
$form->display();