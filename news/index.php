<?php
include_once '../sys/inc/start.php';
$doc = new Document();

$doc->title = 'Главные трансферные новости';

if(isset($_GET['type'])) {
    if($_GET['type'] == 'off') {
        $where = 'WHERE `off_news` = 1';
        $doc->title = 'Официальные переходы';
    }
    if($_GET['type'] == 'lenta') {
        $where = 'WHERE `type` = 1';
        $doc->title = 'Все новости и слухи';
    }
    if($_GET['type'] == 'top') {
        $time_top = TIME - 60 * 60 * 24 * 7;
        $where = 'WHERE `time` > ' . $time_top;
        $doc->title = 'Популярное за неделю';
    }
} else {
    $where = 'WHERE `type` = 0';
}


$pages = new Pages();
$pages->st = 3;
$pages->items_per_page = 12;
$res = Db::me()->query("SELECT COUNT(*) FROM `news` $where");
$pages->posts = $res->fetchColumn(); // количество сообщений

$res = Db::me()->query("SELECT * FROM `news` $where ORDER BY `id` DESC LIMIT " . $pages->limit);


$listing = new Listing('news.php');

foreach($res->fetchAll() as $news) {
    $post = $listing->post();

    $post->title = Text::substr($news['title'], 77);
    $post->url = "/news/" . Text::urlToString($news['id'] . "-" . $news['title']);
    $post->img = "/sys/files/news/original/" . $news['img'];
    $post->text = Text::substr($news['text'], 190);
    $post->views = $news['views'];
    $post->comments = $news['comments'];
    $post->time = $news['time'];
    $post->off_news = $news['off_news'];
}


$listing->display('Нет нововтей', 'textView.php');

$pages->display('?'); // вывод страниц