<?php
include_once '../sys/inc/start.php';
$doc = new Document(2);
$doc->title = 'Удалить новость';




if(!isset($_GET['id'])) {
    $doc->accessDenied("Обращение к несуществующей новости");
}

$id = (int)$_GET['id'];

$res = Db::me()->prepare("SELECT * FROM `news` WHERE `id` = :id LIMIT 1");
$res->execute(array(':id' => $id));

$news = $res->fetch();

if(!$news) {
    $doc->accessDenied("Обращение к несуществующей новости");
}



if(isset($_GET['del'])) {
    $res = Db::me()->prepare("DELETE FROM `news` WHERE `id` = ?");
    $res->execute(Array($id));

    $res = Db::me()->prepare("DELETE FROM `news_comm` WHERE `id_news` = ?");
    $res->execute(Array($id));

    $sitemap = new Sitemap();
    $sitemap->createMap();
    $sitemap->saveMapXml();

    header("Location: /news/");

    exit;
}

echo "Будет удалена новость, так же все комментарии которые оносятся к новости <br /><br />";
echo $news['title'];
echo '<br /><br /><a href="?id='.$id.'&del">Удалить новость</a>';



$doc->ret("Новости", "/news/");
$doc->ret(Text::substr($news['title'], 20), "/news/" . Text::urlToString($news['id'] . "-" . $news['title']));