<?php
include_once '../sys/inc/start.php';
$doc = new Document();

$id = (int)$_GET['id'];

$res = Db::me()->prepare("SELECT * FROM `news` WHERE `id` = :id LIMIT 1");
$res->execute(array(':id' => $id));

$news = $res->fetch();

if(!$news) {
    $doc->accessDenied("Обращение к несуществующей новости");
}

$res = Db::me()->query("UPDATE `news` SET `views` = `views` + 1 WHERE `id` = '$id'");

$doc->title = $news['title'];
$doc->description = $des = Text::substr($news['text'], 80, 0, "...") . ' ' . $news['tags'];
$doc->keywords = explode(", ", $news['tags']);

$doc->setMetaOg('url', "/news/" . Text::urlToString($news['id'] . "-" . $news['title']));
$doc->setMetaOg('image', "/sys/files/news/original/" . $news['img']);
$doc->setMetaOg('title', Text::toValue($news['title']));
$doc->setMetaOg('description', Text::toValue($des));




$listing = new Listing('newsView.php');

$post = $listing->post();

$post->title = $news['title'];
$post->views = $news['views'];
$post->time = Misc::whenTime($news['time']);
$post->text = Text::toOutput($news['text']);

if($news['img']) {
    $post->img = "/sys/files/news/original/" . $news['img'];
}

$post->tags = "";

$tags_arr = explode(", ", $news['tags']);

for($i = 0; $i < count($tags_arr); $i++) {
    $tag = $tags_arr[$i];
    $tag_url = Text::replaceSpaces($tag);
    if($i == count($tags_arr) - 1) {
        $post->tags .= '<a href="tag/' . $tag_url . '">' . $tag . '</a>';
    } else {
        $post->tags .= '<a href="tag/' . $tag_url . '">' . $tag . '</a>; ';
    }
}

if(!$tags_arr[0]) {
    $post->tags = 'Генерация тегов';
}



if($news['isvideo']) {
    $isvideo = "`isvideo` = 1";
} else {
    $isvideo = "`isvideo` = 0";
}

$res = Db::me()->query("SELECT * FROM `news` WHERE $isvideo ORDER BY `id` DESC LIMIT 6");

$more_news = array();

foreach($res->fetchAll() AS $key => $news) {
    $more_news[] = array(
        "title" => Text::substr($news['title'], 77),
        "url" => "/news/" . Text::urlToString($news['id'] . "-" . $news['title']),
        "img" => "/sys/files/news/mini/" . $news['img'],
        "text" => Text::substr($news['text'], 190),
        "time" => Misc::whenTime($news['time'])
    );
}

$post->more_news = $more_news;



$listing->display();




//if($user->group > 3) {
//    $doc->act('Редактировать', '/dpanel/news.edit.php?id=' . $id);
//    $doc->act('Удалить новость', '/news/news.delete.php?id=' . $id);
//}