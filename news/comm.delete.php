<?php
include_once '../sys/inc/start.php';
$doc = new Document(6);
$doc->title = 'Удалить поста';

$id = (int) $_GET['id'];

$res = Db::me()->prepare("SELECT * FROM `news_comm` WHERE `id` = :id LIMIT 1");
$res->execute(array(':id' => $id));

$comm = $res->fetch();

if(!$comm) {
    $doc->accessDenied("Обращение к несуществующему комментарию");
}


$res = Db::me()->prepare("SELECT * FROM `news` WHERE `id` = :id LIMIT 1");
$res->execute(array(':id' => $comm['id_news']));

$news = $res->fetch();


if(isset($_GET['del'])) {
    $res = Db::me()->prepare("DELETE FROM `news_comm` WHERE `id` = ?");
    $res->execute(Array($id));

    $res = Db::me()->prepare("UPDATE `news` SET `comments` = `comments` - 1 WHERE `id` = ?");
    $res->execute(Array($news['id']));

    header("Location: " . "/news/" . Text::urlToString($news['id'] . "-" . $news['title']));

    exit;
}

echo "Будет удален комментарий <br />";
echo $comm['text'];
echo '<br /><br /><a href="?id='.$id.'&del">Удалить комментарий</a>';


$doc->ret("Новости", "/news/");
$doc->ret(Text::substr($news['title'], 20), "/news/" . Text::urlToString($news['id'] . "-" . $news['title']));