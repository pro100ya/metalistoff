<?php
include_once '../sys/inc/start.php';
$doc = new Document();

$tag = $_GET['tag'];


$search_query_sql = array();

$search_array = preg_split('#-+#u', $_GET['tag']);

for ($i = 0; $i < count($search_array); $i++) {
    $search_query_sql[$i] = '+' . trim($search_array[$i]);
}


$doc->title = $_GET['tag'];

$doc->description = $_GET['tag'] . ": Последние трансферные новости и слухи на сайте посвященном переходам игроков";



$pages = new Pages();
$pages->st = 3;
$res = Db::me()->query("SELECT COUNT(*) FROM `news` WHERE MATCH (`tags`) AGAINST ('".implode(' ', $search_query_sql)."' IN BOOLEAN MODE)");
$pages->posts = $res->fetchColumn(); // количество сообщений

$res = Db::me()->query("SELECT * FROM `news` WHERE MATCH (`tags`) AGAINST ('".implode(' ', $search_query_sql)."' IN BOOLEAN MODE) ORDER BY `id` DESC LIMIT " . $pages->limit);


$listing = new Listing('news.php');

foreach($res->fetchAll() as $news) {
    $post = $listing->post();

    $post->title = Text::substr($news['title'], 77);
    $post->url = "/news/" . Text::urlToString($news['id'] . "-" . $news['title']);
    $post->img = "/sys/files/news/original/" . $news['img'];
    $post->text = Text::substr($news['text'], 190);
    $post->views = $news['views'];
    $post->comments = $news['comments'];
    $post->time = $news['time'];
    $post->off_news = $news['off_news'];
}


$listing->display('Нет нововтей', 'textView.php');

$pages->display('?'); // вывод страниц