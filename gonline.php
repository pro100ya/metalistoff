<?php
include_once 'sys/inc/start.php';

$doc = new Document();
$doc->title = 'Гости онлайн';
$pages = new Pages;
$res = Db::me()->query("SELECT COUNT(*) FROM `guest_online`");
$pages->posts = $res->fetchColumn(); // количество сообщений

$res = Db::me()->query("SELECT * FROM `guest_online` LIMIT " . $pages->limit);

$listing = new Listing();

foreach($res->fetchAll() as $comm) {
    $post = $listing->post();
    $post->title = 'Гость';
    $post->url = "/";
    $post->img = 'Гость';
    $post->time = 11123449;
    if($user->group > 7) {
        $post->text = 'ip: ' . long2ip($comm['ip_long']) . "<br />";
    } else {
        $post->text = '';
    }
}

$listing->display('Нет гостей', 'textView.php');

$pages->display('?'); // вывод страниц