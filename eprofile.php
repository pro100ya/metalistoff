<?php
include_once 'sys/inc/start.php';
$doc = new Document(1);

if(isset($_POST['save'])) {
    $user->first_name = $_POST['first_name'];
    $user->last_name = $_POST['last_name'];
    $user->city = $_POST['city'];
    $user->ank_g_r = $_POST['ank_g_r'];
    $user->ank_m_r = $_POST['ank_m_r'];
    $user->ank_d_r = $_POST['ank_d_r'];
    $user->i_books = $_POST['i_books'];
    $user->i_films = $_POST['i_films'];
    $user->i_teleshow = $_POST['i_teleshow'];
    $user->i_games = $_POST['i_games'];
    $user->i_music = $_POST['i_music'];
    $user->i_commands = $_POST['i_commands'];
    $user->languages = $_POST['languages'];
    $user->about_me = $_POST['about_me'];
    
    $doc->msg('Изменения приняты');
}

$form = new Form();
$form->input("first_name", "Ваше имя", $user->first_name);
$form->input("last_name", "Ваша фамилия", $user->last_name);
$form->input("city", "Город", $user->city);

$options = array();
$options[] = array(0, 'Не выбран');
$year_start = 2009;
for($i = 0; $i < 40; $i++) {
    $options[] = array($year_start, $year_start, ($user->ank_g_r == $year_start));
    $year_start--;
}
$form->select("ank_g_r", "Год рождения", $options);

$options = array();
$options[] = array(0, 'Не выбран');
$options[] = array(1, 'Январь', ($user->ank_m_r == 1));
$options[] = array(2, 'Февраль', ($user->ank_m_r == 2));
$options[] = array(3, 'Март', ($user->ank_m_r == 3));
$options[] = array(4, 'Апрель', ($user->ank_m_r == 4));
$options[] = array(5, 'Май', ($user->ank_m_r == 5));
$options[] = array(6, 'Июнь', ($user->ank_m_r == 6));
$options[] = array(7, 'Июль', ($user->ank_m_r == 7));
$options[] = array(8, 'Август', ($user->ank_m_r == 8));
$options[] = array(9, 'Сентябрь', ($user->ank_m_r == 9));
$options[] = array(10, 'Октябрь', ($user->ank_m_r == 10));
$options[] = array(11, 'Ноябрь', ($user->ank_m_r == 11));
$options[] = array(12, 'Декабрь', ($user->ank_m_r == 12));
$form->select("ank_m_r", "Месяц рождения", $options);

$options = array();
$options[] = array(0, 'Не выбран');
for($i = 1; $i <= 31; $i++) {
    $options[] = array($i, $i, ($user->ank_d_r == $i));
}
$form->select("ank_d_r", "День рождения", $options);

$form->input("languages", "Языки", $user->languages);
$form->input("i_books", "Любимые Книги", $user->i_books);
$form->input("i_films", "Любимые Фильмы", $user->i_films);
$form->input("i_teleshow", "Любимые телешоу", $user->i_teleshow);
$form->input("i_games", "Любимые игры", $user->i_games);
$form->input("i_music", "Любимая музыка", $user->i_music);
$form->input("i_commands", "Любимые футбольные команды", $user->i_commands);
$form->textarea("about_me", "Немного о себе", $user->about_me);

$form->button("Сохранить", "save");
$form->display();

