<?php
include_once 'sys/inc/start.php';
$doc = new Document();
$doc->title = 'Ошибка';

switch ($_GET['err']) {
    case 400:$doc->err('Обнаруженная ошибка в запросе');
        $return = true;
        break;
    case 401:$doc->err('Нет прав для выдачи документа');
        $return = true;
        break;
    case 402:$doc->err('Не реализованный код запроса');
        $return = true;
        break;
    case 403:$doc->err('Доступ запрещен');
        $return = true;
        break;
    case 404:$doc->err('Нет такой страницы');
        $return = true;
        break;
    case 500:$doc->err('Внутренняя ошибка сервера');
        break;
    case 502:$doc->err('Сервер получил недопустимые ответы другого сервера');
        break;
    default: $doc->err('Неизвестная ошибка');
        break;
}