<?php
include_once '../sys/inc/start.php';
$doc = new Document();
$doc->title = 'Гостевая';


if(isset($_POST['save']) && $user->group) {
    $text = Text::substr($_POST['message'], 2000);
    $text = Text::inputText($text);

    if($text) {
        $res = Db::me()->prepare("INSERT INTO `guest` (`id_user`, `text`, `time`) VALUES (?, ?, ?)");
        $res->execute(Array($user->id, $text, TIME));

        $user->balls++;
        $user->guest_comm++;

        $doc->msg('Сообщение отправлено');
    }
}




if($user->id != false) {
    $form = new FormUserComment();
    $form->setUser($user);
    $form->display();
}




$pages = new Pages();
$pages->st = 3;
$res = Db::me()->query("SELECT COUNT(*) FROM `guest`");
$pages->posts = $res->fetchColumn(); // количество сообщений

$res = Db::me()->query("SELECT * FROM `guest` ORDER BY `id` DESC LIMIT " . $pages->limit);

$listing = new Listing();

foreach($res->fetchAll() as $comm) {
    $ank = new User($comm['id_user']);

    $post = $listing->post();
    $post->id = 'chat' . $comm['id'];
    $post->title = $ank->nick;
    $post->url = "/id" . $ank->id;
    $post->text = Text::toOutput($comm['text']);
    $post->time = Misc::when($comm['time']);
    $post->img = $ank->getAvatar();

    $post->action('<a href="?reply='.$comm['id'].'">Ответить</a>');
    $post->action('<a href="post.edit.php?id='.$comm['id'].'">Изменить</a>');
    $post->action('<a href="post.delete.php?id='.$comm['id'].'">Удалить</a>');
}

$listing->display('Пока что нет не одного сообщения', 'textView.php');

$pages->display('?'); // вывод страниц