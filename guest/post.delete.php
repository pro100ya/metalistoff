<?php
include_once '../sys/inc/start.php';
$doc = new Document(6);
$doc->title = 'Удалить поста';

$id = (int) $_GET['id'];

$res = Db::me()->prepare("SELECT * FROM `guest` WHERE `id` = :id LIMIT 1");
$res->execute(array(':id' => $id));

$comm = $res->fetch();

if(!$comm) {
    $doc->accessDenied("Обращение к несуществующему комментарию");
}

if(isset($_GET['del'])) {
    $res = Db::me()->prepare("DELETE FROM `guest` WHERE `id` = ?");
    $res->execute(Array($id));

    header("Location: /guest/");

    exit;
}

echo "Будет удален пост <br />";
echo $comm['text'];
echo '<br /><br /><a href="?id='.$id.'&del">Удалить пост</a>';