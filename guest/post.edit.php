<?php
include_once '../sys/inc/start.php';
$doc = new Document(6);
$doc->title = 'Удалить поста';

$id = (int) $_GET['id'];

$res = Db::me()->prepare("SELECT * FROM `guest` WHERE `id` = :id LIMIT 1");
$res->execute(array(':id' => $id));

$comm = $res->fetch();

if(!$comm) {
    $doc->accessDenied("Обращение к несуществующему комментарию");
}

if(isset($_POST['save'])) {
    $up_news = Db::me()->prepare("UPDATE `guest` SET `text` = ? WHERE `id` = ? LIMIT 1");
    $up_news->execute(Array($_POST['message'], $id));

    header("Location: /guest/");

    exit;
}

$ank = new User($comm['id_user']);

$form = new FormUserComment();
$form->setUser($ank);
$form->setText($comm['text']);
$form->display();