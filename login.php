<?php
include_once 'sys/inc/start.php';
$doc = new Document();
$doc->title = 'Авторизация';

if($user->id) {
    $doc->accessDenied('Вы уже на сайте');
}

if(isset($_POST['save'])) {
    if (isset($_POST['login']) && isset($_POST['password'])) {
        if (!$_POST['login'])
            $doc->err('Введите логин');
        elseif (!$_POST['password'])
            $doc->err('Введите пароль');
        else {
            $login = (string) $_POST['login'];
            $password = (string) $_POST['password'];
            
            $q = Db::me()->prepare("SELECT `id`, `password` FROM `users` WHERE `login` = ? LIMIT 1");
            $q->execute(Array($login));
            if (!$row = $q->fetch()) {
                $doc->err('Логин не зарегистрирован');
            } elseif(Crypt::hash($password) !== $row['password']) {
                $doc->err('Вы ошиблись при вводе пароля');
            } else {
                $id_user = $row['id'];
                $user = new User($id_user);
                
                $_SESSION[SESSION_ID_USER] = $user->id;
                if (isset($_POST['save_to_cookie']) && $_POST['save_to_cookie']) {
                    setcookie(COOKIE_ID_USER, $user->id, TIME + 60 * 60 * 24 * 365);
                    setcookie(COOKIE_USER_PASSWORD, Crypt::encrypt($password), TIME + 60 * 60 * 24 * 365);
                }
                
                $doc->msg('Вы успешно авторезировались');
                header("Location: /profile.php");
                exit();
            }
        }
    }
}


$form = new Form();
$form->input("login", "Ваш логин на сайте");
$form->password("password", "Ваш пароль");
$form->checkbox('save_to_cookie', 'Запомнить меня');
$form->button("Войти", "save");
$form->display();

