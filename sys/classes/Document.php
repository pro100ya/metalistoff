<?php

/**
 * Класс для формирования HTML документа
 */
class Document extends Design
{
    public $title = 'Офф сайт';
    public $description = 'Сайт металлиста';
    public $keywords = array("металист, сайт");
    public $last_modified = null;
    public $tpl_dpanel = false; /* Загружать документ в виде админки */
    protected $messages = array();
    protected $modmsg = array();
    protected $outputed = false;
    protected $actions = array();
    protected $returns = array();
    protected $scripts = array();
    protected $styles = array();
    protected $meta_og = array();

    function __construct($group = 0)
    {
        parent::__construct();
//        global $user;
//        if ($group > $user->group) {
//            $this->accessDenied('Доступ к данной странице запрещен');
//        }
        ob_start();
    }

    public function setMetaOg($tag = '', $value = '') {
        $this->meta_og[$tag] = $value;
    }
    
    function addScript($script = '', $v = 1) {
        $this->scripts[] = array(
            's' => $script,
            'v' => $v
        );
    }
    
    function addStyle($style = '') {
        $this->styles[] = $style;
    }

    /**
     * @param $name
     * @param string|url $url
     * @return array
     */
    function ret($name, $url = '')
    {
        return $this->returns[] = array($name, $url);
    }

    /**
     * @param $name
     * @param string|url $url
     * @return array
     */
    function act($name, $url = '')
    {
        return $this->actions[] = array($name, $url);
    }

    /**
     * @param $text
     * @return document_message
     */
    function err($text)
    {
        $this->messages[] = array($text, true);
    }

    /**
     * @param $text
     * @return document_message
     */
    function msg($text)
    {
        $this->messages[] = array($text);
    }
    
    public function modalMessages($text, $is_error = false)
    {
        $this->modmsg = array($text, $is_error);
    }

    /**
     * Отображение страницы с ошибкой
     * @param string $err Текст ошибки
     */
    public function accessDenied($err)
    {
        $this->err($err);
        $this->output();
        exit;
    }

    /**
     * Формирование HTML документа и отправка данных браузеру
     * @global dcms $dcms
     */
    private function output()
    {
        if ($this->outputed) {
            // повторная отправка html кода вызовет нарушение синтаксиса 
            // документа, да и вообще нам этого нафиг не надо
            return;
        }
        $this->outputed = true;
        
        global $user;

        $this->assign('actions', $this->actions); // ссылки к действию
        $this->assign('returns', $this->returns); // ссылки для возврата

        $this->assign('messages', $this->messages); // сообщения или ошибка
        
        $this->assign('title', $this->title);
        
        $this->assign('user', $user);
        
        $this->assign('scripts', $this->scripts);
        $this->assign('styles', $this->styles);
        
        $description = Text::toValue(Text::substr($this->description, 150));
        $this->assign('description', $description);

        $keywords = implode(", ", $this->keywords);
        $this->assign('keywords', $keywords);

        $this->assign('meta_og', $this->meta_og);
        
        $this->assign('content', ob_get_clean());
        
        $gen_time = round(microtime(true) - TIME_START, 3);
        $this->assign('document_generation_time', $gen_time); // время генерации страницы
        
        header('Cache-Control: no-store, no-cache, must-revalidate', true);
        header('Expires: ' . date('r'), true);
        if ($this->last_modified) {
            header("Last-Modified: " . gmdate("D, d M Y H:i:s", (int) $this->last_modified) . " GMT", true);
        }
        

        header('X-UA-Compatible: IE=edge', true); // отключение режима совместимости в осле
        header('Content-Type: text/html; charset=utf-8', true);

        if($this->tpl_dpanel) {
            $this->display('dpanel/document.php');
        } else {
            $this->display('document.php');
        }

        
    }

    /**
     * Очистка вывода
     * Тема оформления применяться не будет
     */
    function clean()
    {
        $this->outputed = true;
        ob_clean();
    }

    /**
     * То что срабатывает при exit
     */
    function __destruct()
    {
        $this->output();
    }
}