<?php

/**
 * Пользователь
 * @property int id Уникальный идентификатор пользователя
 */
class User
{
    protected $_update = array();
    protected $_data = array();
    protected $_id = 0;

    /**
     * @param boolean|int $id_or_arrayToCache
     * Идентификатор пользователя или массив идентификаторов для запроса из базы и помещения в кэш
     */
    function __construct($id = false, $use_cache = true)
    {
        $this->_id = (int) $id;
        
        if($id === false) {
            $this->_initGuest();
            return;
        }
        
        if($this->_id === 0) {
            $this->_initBot();
            return;
        }
        
        if (!$use_cache) {
            $this->_getUserDataFromBase();
        } else {
            try {
                $this->_getUserDataFromCache();
            } catch (Exception $e) {
                $this->_getUserDataFromBase();
            }
        }
    }
    
    private function _initBot()
    {
        $this->_data = array();
        $this->_data['id'] = 0;
        $this->_data['login'] = '[SystemBot]';
        $this->_data['sex'] = 1;
        $this->_data['group'] = 9;
        $this->_data['color_login'] = '';
    }

    private function _initGuest()
    {
        $this->_data = array();
        $this->_data['id'] = false;
        $this->_data['sex'] = 1;
        $this->_data['group'] = 0;
    }

    protected function _getUserDataFromBase()
    {
        $this->_initGuest();
        
        $res = Db::me()->prepare("SELECT * FROM `users` WHERE `id` = :id_user LIMIT 1");
        $res->execute(array(':id_user' => $this->_id));
        $data = $res->fetch();
        if (!$data) {
            $this->_data['login'] = '[[Удален]]';
            return;
        }
        $this->_data = $data;
        $this->_saveUserDataToCache();
    }
    
    protected function _getUserDataFromCache()
    {
        $data = Cache::get('User.'.$this->_id, false);
        if (!$data) {
            throw new Exception('Не удалось получить данные пользователя ');
        }
        $this->_data = $data;
    }
    
    protected function _saveUserDataToCache()
    {
        Cache::set('User.'.$this->_id, $this->_data, 10);
    }
    
    private function _grupName() {
        $groups_json = Json::decode(file_get_contents(H . "/sys/settings/users_groups.json"));
        
        return $groups_json[$this->_data['group']]['name'];
    }

    /**
     *
     * @global \dcms $dcms
     * @param string $n ключ
     * @return mixed значение
     */
    function __get($n)
    {
        switch ($n) {
            case 'online' :
                return (bool)($this->_data ['last_visit'] > TIME - SESSION_LIFE_TIME);
            case 'group_name' :
                return $this->_grupName();
            case 'login' :
                return $this->_data ['login'];
            case 'nick' :
                return $this->_data ['login'];
            default :
                return !isset($this->_data [$n]) ? false : $this->_data [$n];
        }
    }

    /**
     *
     * @global \dcms $dcms
     * @param string $n ключ
     * @param string $v значение
     */
    function __set($n, $v)
    {
        if (empty($this->_data['id']))
            return;
        switch ($n) {
            case 'theme' :
                $n .= "sdd";
                break;
        }

        if (isset($this->_data[$n])) {
            $this->_data[$n] = $v;
            $this->_update[$n] = $v;
        }
    }

    /**
     * Вернет аватар пользователя, всразу ввиде html картинки
     * @param int $size
     * @param bool $dphoto
     * @return string
     */
    public function getAvatar($size = 50, $dphoto = false)
    {
        $fol = 'folder' . substr($this->_id, -1);
        
        switch ($size) {
            case 200:
                $file = '/sys/files/avatars/photo_200/'.$fol.'/' . $this->_id .'.jpg';
                $wh = 'width="200"';
                $prefix = 200;
                if($dphoto) {
                    $data_photo = 'data-photo="/sys/files/avatars/photo_original/'.$fol.'/' . $this->_id .'.jpg?"';
                }
                break;
            case 100:
                $file = '/sys/files/avatars/photo_100/'.$fol.'/' . $this->_id .'.jpg';
                $wh = 'width="100" height="100"';
                $prefix = 100;
                break;
            case 50:
                $file = '/sys/files/avatars/photo_50/'.$fol.'/' . $this->_id .'.jpg';
                $wh = 'width="50" height="50"';
                $prefix = 50;
                break;
            case 30:
                $file = '/sys/files/avatars/photo_50/'.$fol.'/' . $this->_id .'.jpg';
                $wh = 'width="30" height="30"';
                $prefix = 50;
                break;
            default:
                $file = '/sys/files/avatars/photo_50/'.$fol.'/' . $this->_id .'.jpg';
                $wh = 'width="50" height="50"';
                $prefix = 50;
                break;
        }

        if(is_file(H . $file)) {
            $img = '<img '.(isset($data_photo) ? 'class="viewimg" ' . $data_photo : '').' src="'.$file.'" alt="!" '.$wh.' />';
        } else {
            $img = '<img src="/sys/files/avatars/photo_'.$prefix.'/0.jpg" alt="" '.$wh.' />';
        }

        return $img;
    }

    public function saveData()
    {
        if ($this->_update) {
            $sql = array();
            foreach ($this->_update as $key => $value) {
                $sql[] = "`" . $key . "` = " . Db::me()->quote($value);
            }
            Db::me()->query("UPDATE `users` SET " . implode(', ', $sql) . " WHERE `id` = '" . $this->_data['id'] . "' LIMIT 1");
            $this->_update = array();
        }
    }

    function __destruct()
    {
        $this->saveData();
    }

}