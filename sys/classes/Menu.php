<?php

class Menu
{
    static public 
            $_menu = array(),
            $_remeve = array(),
            $_json_config = array();


    static public function section($section = '')
    {
        if(!self::$_json_config) {
            $file_j = file_get_contents(H . "/sys/themes/default_full/menu.json");
            self::$_json_config = Json::decode($file_j, true);
        }

        $content = '';

        if(!self::$_json_config[$section]) {
            return $content;
        }

        if(isset(self::$_remeve[$section])) {
            return $content;
        }

        //global $user;

        $menu = self::$_json_config[$section];

        if($section === 'user') {
            $content = self::_menuUser($menu);

            echo $content;

            return true;
        }

        $design = new Design();
        $design->assign($menu);
        //$design->assign('user', $user);
        $content = $design->fetch($menu['tpl']);

        echo $content;
    }
    
    public static function removeMenu($name)
    {
        self::$_remeve[$name] = 1;
    }

    public static function _menuUser($menu)
    {
        global $user;

        if($user->id !== false) {
            $design = new Design();
            $design->assign($menu);
            $design->assign('user', $user);
            $content = $design->fetch($menu['tpl']);
        } else {
            $form = new Form("/login.php");
            $form->html('<div style="color: #e6e6eb;">Ваш логин на сайте</div>');
            $form->input("login", "");
            $form->html('<div style="color: #e6e6eb;">Ваш пароль</div>');
            $form->password("password", "");
            $form->checkbox('save_to_cookie', 'Запомнить меня');
            $form->button("Войти", "save");
            $content = $form->fetch();

            $content .= '<a style="display: block; padding: 6px 8px; font-size: 16px; color: #e6e6eb; text-align: center; margin-top: 6px; background-color: #616161;" href="/reg.php">Регистрация</a>';
        }



        return $content;
    }
}

