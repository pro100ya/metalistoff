<?php

class Browser
{
    static function getIpLong()
    {
        static $ipLong = false;
        if ($ipLong === false) {
            $ipLong = sprintf("%u", ip2long($_SERVER['REMOTE_ADDR']));
        }
        return $ipLong;
    }
}

