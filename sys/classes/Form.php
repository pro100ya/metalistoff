<?php

/**
 * Генератор форм
 */
class Form
{

    private $_tpl_file = '',
            $_data = array(),
            $_id_eliment = 1;
    /**
     * Создание формы
     * @param string|url $url Путь (атрибут action в форме)
     * @param boolean $post true-отправлять post`ом, false - get`ом
     */
    public function __construct($url = '', $post = true)
    {
        $this->_tpl_file = 'form.php';

        $this->_data['el'] = array();

        $this->setUrl($url);
        $this->setMethod($post ? 'post' : 'get');
    }
    
    /**
     * Вставка HTML блока
     * @param string $html
     * @param boolean $br
     */
    function html($html, $br = false)
    {
        $this->_data['el'][] = array(
            'type' => 'html',
            'br' => (bool)$br,
            'value' => $html
        );
    }

    /**
     * Чекбокс
     * @param string $name аттрибут name
     * @param string $title текст к чекбоксу
     * @param boolean $checked значение, установлена ли галочка
     * @param boolean $br перенос строки
     * @param string $value аттрибут value
     */
    function checkbox($name, $title, $checked = false, $br = true, $value = '1')
    {
        $this->_data['el'][] = array(
            'type' => 'checkbox',
            'br' => (bool)$br,
            'info' => array(
                'name' => $name,
                'checked' => (bool)$checked,
                'value' => $value,
                'text' => $title,
                'id' => $this->_id_eliment
            )
        );

        $this->_id_eliment++;
    }

    /**
     * Поле "select"
     * @param string $name
     * @param string $title
     * @param array $options
     * @param boolean $br
     */
    function select($name, $title, $options, $br = false)
    {
        $this->_data['el'][] = array(
            'type' => 'select',
            'title' => $title,
            'br' => (bool)$br,
            'info' => array(
                'name' => $name,
                'options' => (array)$options
            )
        );
    }

    /**
     * Кнопка
     * @param string $text Отображаемое название кнопки
     * @param string $name аттрибут name
     * @param boolean $br перенос
     */
    function button($text, $name = '', $br = true)
    {
        $this->input($name, '', $text, 'submit', $br);
    }

    /**
     * Поде для выбора файла
     * @param string $name аттрибут name
     * @param string $title Заголовок к полю выбора файла
     * @param boolean $br перенос строки
     */
    function file($name, $title, $br = false)
    {
        $this->input($name, $title, false, 'file', $br);
    }

    /**
     * Капча
     * @param boolean $br перенос строки
     */
    function captcha($br = true)
    {
        $this->_data['el'][] = array('type' => 'captcha', 'br' => $br, 'session' => captcha::gen());
    }

    /**
     * Поле ввода пароля
     * @param string $name аттрибут name
     * @param string $title Заголовок к полю ввода
     * @param string $value введенное значение в поле
     * @param boolean $br перенос строки
     * @param bool|int $size ширина поля ввода в символах
     */
    function password($name, $title, $value = '', $br = true, $size = false)
    {
        $this->input($name, $title, $value, 'password', $br, $size);
    }

    /**
     * Текстовое поле ввода
     * @param string $name аттрибут name
     * @param string $title Заголовок поля ввода
     * @param string $value значение в поле ввода
     * @param boolean $br перенос строки
     * @param bool|int $size ширина поля ввода в символах
     * @param boolean $disabled запретить изменение
     */
    function text($name, $title, $value = '', $br = true, $size = false, $disabled = false)
    {
        $this->input($name, $title, $value, 'text', $br, $size, $disabled);
    }

    /**
     * Скрытое поле формы
     * @param string $name аттрибут name
     * @param string $value значение
     */
    function hidden($name, $value)
    {
        $this->input($name, '', $value, 'hidden', false);
    }

    /**
     * Поле ввода для сообщения
     * @param string $name аттрибут name
     * @param string $title заголовок поля ввода
     * @param string $value введенный текст
     * @param bool $submit_ctrl_enter отправка формы по Ctrl + Enter
     * @param boolean $br перенос
     * @param boolean $disabled запретить изменение
     */
    function textarea($name, $title = '', $value = '', $br = false, $disabled = false)
    {
        $this->_data['el'][] = array(
            'type' => 'textarea',
            'title' => $title,
            'br' => (bool)$br,
            'info' => array(
                'name' => $name,
                'value' => $value,
                'disabled' => (bool)$disabled
            ));
        
        
        $this->setBbcodeDisplay();
        
    }

    /**
     * Добавление input`a
     * @param string $name аттрибут name
     * @param string $title заголовок
     * @param string $value значение по-умолчанию
     * @param string $type тип (аттрибут type)
     * @param boolean $br вставка переноса строки после input`a
     * @param bool|int $size ширина поля ввода в символах
     * @param boolean $disabled блокировать изменения
     * @param bool|int $maxlength максимальная вместимость в символах
     * @return boolean
     */
    function input($name, $title, $value = '', $type = 'text', $br = true, $size = false, $disabled = false, $maxlength = false)
    {
        if (!in_array($type, array('text', 'input_text', 'password', 'hidden', 'textarea', 'submit', 'file')))
            return false;

        $input = array();

        if ($type == 'file')
            $this->setIsFiles();

        $input['type'] = $type;
        $input['title'] = $title;
        $input['br'] = (bool)$br;

        $info = array();
        $info['name'] = $name;
        $info['value'] = $value;

        $info['disabled'] = (bool)$disabled;

        if ($size)
            $info['size'] = (int)$size;
        if ($maxlength)
            $info['maxlength'] = (int)$maxlength;

        $input['info'] = $info;
        $this->_data['el'][] = $input;
        return true;
    }
    
    public function setBbcodeDisplay($list = false) {
        if($list === false) {
            $list = array(
                array('teg' => 'b', 'title' => 'Жирный', 'class' => 'bold'),
                array('teg' => 'u', 'title' => 'Подчеркнутый', 'class' => 'underlined'),
                array('teg' => 'i', 'title' => 'Курсив', 'class' => 'italic'),
                array('teg' => 'quote', 'title' => 'Цитата', 'class' => 'quote'),
                array('teg' => 'center', 'title' => 'Тескст по центру', 'class' => 'center'),
                array('teg' => 'right', 'title' => 'Текст по правому краю', 'class' => 'right'),
                array('teg' => 'left', 'title' => 'Текст по левому краю', 'class' => 'left'),
                array('teg' => 'smiles', 'title' => 'Смайлы сайта', 'class' => 'smiles', 'right' => true),
                array('teg' => 'url', 'title' => 'Вставка ссылки', 'class' => 'url', 'right' => true),
                array('teg' => 'img', 'title' => 'Изображение', 'class' => 'img', 'right' => true),
                array('teg' => 'video', 'title' => 'Видео', 'class' => 'video', 'right' => true),
                array('teg' => 'spoiler', 'title' => 'Спойлер', 'class' => 'spoiler', 'right' => true)
            );
        } else {
            $list = $list;
        }
        
        $this->_data['bbcode'] = $list;
    }
    
    public function setClass($class) {
        $this->_data['class'] = $class;
    }

    /**
     * Установка метода передачи формы на сервер (post, get)
     * @param string $method
     */
    function setMethod($method)
    {
        if (in_array($method, array('get', 'post')))
            $this->_data['method'] = $method;
    }

    /**
     * Установка URL (атрибут action формы)
     * @param string|url $url
     */
    function setUrl($url)
    {
        $this->_data['action'] = (string)$url;
    }

    /**
     * Передать форме пользователя все данные
     * @param bool $user
     */
    public function setUser($user = false)
    {
        $this->_data['user'] = $user;
    }

    /**
     * Будут передаваться файлы
     */
    function setIsFiles()
    {
        $this->_data['method'] = 'post';
        $this->_data['files'] = true;
    }

    /**
     * Отдать форму в виде html
     * @param string $tpl_file
     * @return string
     */
    public function fetch($tpl_file = '')
    {
        if($tpl_file) {
            $this->_tpl_file = $tpl_file;
        }

        $design = new Design();
        $design->assign($this->_data);
        return $design->fetch($this->_tpl_file);
    }

    /**
     * Вывод формы
     * @param string $tpl_file
     */
    public function display($tpl_file = '')
    {
        echo $this->fetch($tpl_file);
    }

}