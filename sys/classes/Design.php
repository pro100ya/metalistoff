<?php

/**
 * Дизайн. Конфигуратор шаблонизатора.
 */
class Design extends NativeTemplating
{

    public $theme;

    function __construct()
    {
        parent::__construct();
        

        //$this->theme = $theme;

        // папка шаблонов
        //$this->_dir_templates = H . '/sys/themes/' . $theme->getName() . '/tpl/';

        // системные переменные
        //$this->assign('path', '/sys/themes/' . $theme->getName());
    }

    /**
     * Максимальная ширина изображения в зависимости от типа браузера и параметров темы
     */
    function img_max_width()
    {
        return $this->theme->getImgWidthMax();
    }

    /**
     * Ищет путь к указанной иконке.
     * @param string $name Имя иконки
     * @return string Путь к иконке
     */
    function getIconPath($name)
    {
        return '/sys/images/icons/' . basename($name, '.png') . '.png';
    }
}