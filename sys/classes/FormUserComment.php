<?php

/**
 * Created by PhpStorm.
 * User: ZeroTwink
 * Date: 21.03.2016
 * Time: 18:44
 */
class FormUserComment
{
    private $_tpl_file = '',
            $_data = array();

    /**
     * Создание формы
     * @param string|url $url Путь (атрибут action в форме)
     * @param boolean $post true-отправлять post`ом, false - get`ом
     */
    public function __construct($url = '', $post = true)
    {
        $this->_tpl_file = 'form.userComment.php';

        $this->_data['el'] = array();

        $this->setTypeComment();
        $this->setText();

        $this->setUrl($url);
        $this->setMethod($post ? 'post' : 'get');
    }

    /**
     * Выводить ли выбор Положительный отрицательный комментарий
     * @param bool $type
     */
    public function setTypeComment($type = false)
    {
        $this->_data['type'] = $type;
    }

    /**
     * Текст в texarea
     * @param string $text
     */
    public function setText($text = '')
    {
        $this->_data['text'] = $text;
    }

    /**
     * Установка метода передачи формы на сервер (post, get)
     * @param string $method
     */
    function setMethod($method)
    {
        if (in_array($method, array('get', 'post')))
            $this->_data['method'] = $method;
    }

    /**
     * Установка URL (атрибут action формы)
     * @param string|url $url
     */
    function setUrl($url)
    {
        $this->_data['action'] = (string)$url;
    }

    /**
     * Передать форме пользователя все данные
     * @param bool $user
     */
    public function setUser($user = false)
    {
        $this->_data['user'] = $user;
    }

    /**
     * Отдать форму в виде html
     * @param string $tpl_file
     * @return string
     */
    public function fetch($tpl_file = '')
    {
        if($tpl_file) {
            $this->_tpl_file = $tpl_file;
        }

        $design = new Design();
        $design->assign($this->_data);
        return $design->fetch($this->_tpl_file);
    }

    /**
     * Вывод формы
     * @param string $tpl_file
     */
    public function display($tpl_file = '')
    {
        echo $this->fetch($tpl_file);
    }
}