<?php

/**
 * Created by PhpStorm.
 * User: ZeroTwink
 * Date: 17.04.2016
 * Time: 15:41
 */
class Sitemap
{
    private $_xml = false;

    public function __construct()
    {

    }

    public function createMap()
    {
        //Нужно для даты
        define('DATE_FORMAT_RFC822','r');
        // Создаем документ
        $this->_xml = $xml = new DomDocument('1.0','utf-8');



        //Заголовки
        $urlset = $xml->appendChild($xml->createElement('urlset'));
        $urlset->setAttribute('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
        $urlset->setAttribute('xsi:schemaLocation','http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
        $urlset->setAttribute('xmlns','http://www.sitemaps.org/schemas/sitemap/0.9');

        $res = Db::me()->query("SELECT * FROM `news` ORDER BY `id` DESC LIMIT 100");
        foreach($res->fetchAll() as $news) {
            $url = $urlset->appendChild($xml->createElement('url'));
            $loc = $url->appendChild($xml->createElement('loc'));
            $changefreq = $url->appendChild($xml->createElement('changefreq'));
            $priority = $url->appendChild($xml->createElement('priority'));
            $loc->appendChild($xml->createTextNode('http://' . $_SERVER['HTTP_HOST'] .
                "/news/" . Text::urlToString($news['id'] . "-" . $news['title'])));
            $changefreq->appendChild($xml->createTextNode('monthly'));
            $priority->appendChild($xml->createTextNode('0.9'));
        }

        $xml -> formatOutput = true;
    }

    public function saveMapXml()
    {
        $this->_xml->save(H . '/sitemap.xml');
    }
}