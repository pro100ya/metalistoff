<?php

/**
 * UI. Пост в списке постов
 * @property mixed post
 */
class ListingPost
{

    public $_data = array();
    private $_tpl_name = 'post.php';
    /**
     * @param string $title заголовок поста
     * @param string $content Содержимое поста
     */
    public function __construct($tpl = 'post.php')
    {
        $this->_tpl_name = $tpl;
        
        $this->_data['actions'] = array();
    }

    public function __get($name)
    {
        return $this->_data[$name];
    }
    
    public function __set($name, $value)
    {
        $this->_data[$name] = $value;
    }
    
    public function fetch()
    {
        $design = new Design();
        $design->assign($this->_data);
        $con = $design->fetch($this->_tpl_name);
        
        return $con;
    }

    /**
     * Действия
     * @param string $html
     * @param string|url $url путь
     * @param right bool Вывод с левой стороны
     */
    public function action($html = '', $url = '', $right = false)
    {
        $this->_data['actions'][] = array('html' => $html, 'url' => $url, 'right' => $right);
    }
}