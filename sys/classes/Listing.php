<?php

class Listing {

    private $_list = array(),
            $_tpl = 'post.php';
    
    public function __construct($tpl = '') {
        if($tpl) {
            $this->_tpl = $tpl;
        }
    }


    public function post($tpl = '')
    {
        if($tpl) {
            $this->_tpl = $tpl;
        }
        return $this->_list[] = new ListingPost($this->_tpl);
    }
    
    /**
     * получение контента
     * @param string $text_if_empty Текст, отображаемый при отсутствии пунктов
     * @return string
     */
    public function fetch($text_if_empty = '', $tpl = '')
    {
        $content = '';
        
        if($text_if_empty && !$this->_list) {
            $design = new Design();
            $design->assign('title', $text_if_empty);
            if($tpl) {
                $demo_tpl = $tpl;
            } else {
                $demo_tpl = $this->_tpl;
            }
            $content = $design->fetch($demo_tpl);
        } else {
            foreach ($this->_list as $post) {
                $content .= $post->fetch();
            }
        }
        
        return $content;
    }

    /**
     * отображение
     * @param string $text_if_empty Текст, отображаемый при отсутствии пунктов
     */
    public function display($text_if_empty = '', $tpl = '')
    {
        echo $this->fetch($text_if_empty, $tpl);
    }

}