<div id="block_news_video">
    <div class="block_video_title">
        Новости видео
    </div>

    <div class="items_wrap clear_fix">
        <?php foreach ($video_news AS $news) { ?>
        <div class="item fl_l">
            <div class="item_time">
                <span class="fa fa-clock-o"></span> <?= $news['time'] ?>
                <span class="fa fa-video-camera fl_r"></span>
            </div>
            <div class="img_wrap">
                <img src="/1.jpg" alt="" />
            </div>
            <div class="item_title">
                <a href="<?= $news['url'] ?>"><?= $news['title'] ?></a>
            </div>
        </div>
        <?php } ?>
    </div>
</div>