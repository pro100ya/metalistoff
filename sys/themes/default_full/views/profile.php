<div id="profile">
    <div id="top_bg" style="background: url(/sys/images/profile_bg/original/1.jpg)">
        <div id="is_online">
            <?= $top['online'] ?>
        </div>
        <div id="is_nick">
            <?= $top['nick'] ?>
        </div>
        <div id="profile_image_wrapper">
            <div id="profile_image">
                <?php if ($other['edit_avatar']) { ?>
                    <div id="img_buttons">
                        <div onclick="Profile.avatarNew(this, event)">Загрузить новый аватар</div>
                        <div onclick="Profile.avatarResize('/sys/files/avatars/photo_original/folder<?= substr($ank->id, -1) ?>/<?= $ank->id ?>.jpg', event); return false;">Изменить область</div>
                    </div>
                <?php } ?>
                <?= $img ?>
            </div>
        </div>
    </div>
    <div id="counters_line">
        <div class="item">
            <div class="title">
                Активность
            </div>
            <div class="counter">
                <?= (int) $ank->activity ?>
            </div>
        </div>
        <div class="item">
            <div class="title">
                Карма
            </div>
            <div class="counter">
                <?= $ank->karma ?>
            </div>
        </div>
        <div class="item">
            <div class="title">
                Баллы
            </div>
            <div class="counter">
                <?= $ank->balls ?>
            </div>
        </div>
    </div>
    <div id="profile_table">
        <div id="profile_left_t">
            <div id="profile_content">
                <div style="font-size: 16px; text-align: center; color: #A5A5A5;">
                    <?= $top['names'] ?>
                </div>
            </div>
        </div>
        <div id="profile_right_t">
            <?php foreach ($info as $value) { ?>
                <?php if(isset($value[2])) { ?>
                    <div style="background-color: #F6F6F6; padding: 6px 4px 6px 14px; margin: 8px 0;"><?= $value[0] ?></div>
                    <?php continue; ?>
                <?php } ?>
                <div class="wrapper_lable clear_fix">
                    <div class="lable fl_l">
                        <?= $value[0] ?>:
                    </div>
                    <div class="labeled fl_l">
                        <?= $value[1] ?>
                    </div>
                </div>
            <?php } ?>
            <?= $wall ?>
        </div>
    </div>
</div>