<div class="form">
    <form class="<?= (!isset($class)? 'main_form' : $class) ?>"
            <?= ($method ? ' method="' . $method . '"' : '') ?>
            action="<?= ($action ? $action : '') ?>"
            <?= (isset($files) ? ' enctype="multipart/form-data"' : '') ?>>
        
        <?php
        foreach ($el AS $element) {
            if (!empty($element['title']))
                echo '<div class="form_title">' . $element['title'] . ':</div>';
            switch ($element['type']) {
                case 'html':
                    echo $element['value'];
                    break;
                case 'text':
                    echo '<input class="form_control" type="text"' .
                    (!empty($element['info']['name']) ? ' name="' . $element['info']['name'] . '"' : '') .
                    (!empty($element['info']['value']) ? ' value="' . Text::toValue($element['info']['value']) . '"' : '') .
                    (!empty($element['info']['maxlength']) ? ' maxlength="' . intval($element['info']['maxlength']) . '"' : '') .
                    (!empty($element['info']['size']) ? ' size="' . intval($element['info']['size']) . '"' : '') .
                    (!empty($element['info']['disabled']) ? ' disabled="disabled"' : '') .
                    ' />';
                    break;
                case 'password':
                    echo '<input class="form_control" type="password"' .
                    (!empty($element['info']['name']) ? ' name="' . $element['info']['name'] . '"' : '') .
                    (!empty($element['info']['value']) ? ' value="' . $element['info']['value'] . '"' : '') .
                    (!empty($element['info']['maxlength']) ? ' maxlength="' . intval($element['info']['maxlength']) . '"' : '') .
                    (!empty($element['info']['size']) ? ' size="' . intval($element['info']['size']) . '"' : '') .
                    (!empty($element['info']['disabled']) ? ' disabled="disabled"' : '') .
                    ' />';
                    break;
                case 'hidden':
                    echo '<input type="hidden"' .
                        (!empty($element['info']['name']) ? ' name="' . $element['info']['name'] . '"' : '') .
                        (!empty($element['info']['value']) ? ' value="' . Text::toValue($element['info']['value']) . '"' : '') .
                        ' />';
                    break;
                case 'textarea':
                    echo '<div class="wrap_textarea">';
                        if($bbcode) {
                            echo '<ul class="redactor_toolbar">';
                                foreach ($bbcode as $value) {
                                    echo '<li class="'.(isset($value['right'])? "fl_r" : "fl_l").'"><span title="'.$value['title'].'" data-teg="'.$value['teg'].'" class="redactor_'.$value['class'].'"></span></li>';
                                }
                            echo '</ul>';
                        }
                        echo '<textarea' .
                        (!empty($element['info']['name']) ? ' name="' . $element['info']['name'] . '"' : '') .
                        (!empty($element['info']['disabled']) ? ' disabled="disabled"' : '') .
                        ' class="textarea_animate">' .
                        (!empty($element['info']['value']) ? Text::toValue($element['info']['value']) : '') .
                        '</textarea>';
                    echo '</div>';
                    break;
                case 'checkbox':
                        echo '<div class="wrapper_checkbox_b">';
                            echo '<div class="item_checkbox_b">';
                                echo '<input type="checkbox"' .
                                    ($element['info']['name'] ? ' name="' . $element['info']['name'] . '"' : '') .
                                    ($element['info']['value'] ? ' value="' . Text::toValue($element['info']['value']) . '"' : '') .
                                    ($element['info']['checked'] ? ' checked="checked"' : '') .
                                    ($element['info']['id'] ? ' id="my_checkbox'. $element['info']['id'] .'"' : '') . '/>';
                                echo '<label for="my_checkbox'. $element['info']['id'] .'">' .
                                    ($element['info']['text'] ? ' ' . $element['info']['text'] : '') . '</label>';
                            echo '</div>';
                        echo '</div>';
                    break;
                case 'submit':
                    echo '<input type="submit"' .
                    ($element['info']['name'] ? ' name="' . $element['info']['name'] . '"' : '') .
                    ($element['info']['value'] ? ' value="' . Text::toValue($element['info']['value']) . '"' : '') .
                    ' />';
                    break;
                case 'file':
                    echo '<div class="fileform">';
                    echo '<div class="textfile">Файл не выбран</div>';
                    echo '<div class="selectbutton">Обзор</div>';
                    echo '<input class="upload" type="file"' . 
                            ($element['info']['name'] ? ' name="' . $element['info']['name'] . '"' : '') . 
                            ' />';
                    echo '</div>';
                    break;
                case 'select':
                    $action = $element['info']['options'][0][0];
                    $start_title = $element['info']['options'][0][1];

                    $all_options = '';
                    foreach ($element['info']['options'] AS $option) {
                        if(!empty($option[2])) {
                            $action = $option[0];
                            $start_title = $option[1];
                            $all_options .= '<li data-value="'.$option[0].'">'.$option[1].'</li>';
                        } else {
                            $all_options .= '<li data-value="'.$option[0].'">'.$option[1].'</li>';
                        }
                    }

                    echo '<div class="myselect_wrapper">';
                        echo '<div class="myselect_select">';
                            echo '<div class="myselect_select_text">'.$start_title.'</div>';
                            echo '<div class="myselect_select_trigger">';
                                echo '<div class="myselect_select_trigger_arrow"></div>';
                            echo '</div>';
                        echo '</div>';
                        echo '<div class="myselect_dropdown">';
                            echo '<ul>';
                                echo '<input value="'.Text::toValue($action).'" name="'.$element['info']['name'].'"
                                type="text" style="opacity: 0; display: none;" />';
                                echo $all_options;
                            echo '</ul>';
                        echo '</div>';
                    echo '</div>';
                    break;
            }
            
            if ($element['br'])
                echo '<br />';
        }
        ?>
    </form>
</div>