<div id="<?= $id ?>" class="post">
    <div class="post_img">
            <a href="<?= $url ?>">
            <?= $img ?>
        </a>
    </div>
    <div class="post_title">
        <a href="<?= $url ?>"><?= $title ?></a>
        <div class="post_time">
            - <?= $time ?>
        </div>
        <?php if(isset($type)) { ?>
            <div class="fl_r <?= (!$type)? 'post_type_good' : 'post_type_bad' ?>"></div>
        <?php } ?>
    </div>
    <?= $text ?>
    <div class="post_action">
        <?php foreach($actions AS $act) { ?>
            <div class="item">
                <?= $act['html'] ?>
            </div>
        <?php } ?>
    </div>
</div>