<div class="form_user_comment">
    <form method="post" action="">
        <div class="for_text">
            <div class="image">
                <?= $user->getAvatar() ?>
            </div>
            <textarea class="message textarea_animate" name="message"><?= $text ? $text : '' ?></textarea>
            <?php if($type) { ?>
                <div class="wrapper_radio_b">
                    <div class="item_radio_b">
                        <input name="type_comm" id="my_radio" type="radio" value="0" checked="checked" />
                        <label for="my_radio">Положительный</label>
                    </div>
                    <div class="item_radio_b">
                        <input name="type_comm" id="my_radio1" type="radio" value="1" />
                        <label for="my_radio1">Отрицательный</label>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="for_button">
            <input type="submit" name="save" value="Отправить" />
            <input type="reset" value="Очистить" />
        </div>
    </form>
</div>