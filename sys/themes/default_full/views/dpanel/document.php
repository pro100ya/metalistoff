<?php
/**
 * @var $this document
 */
?><!DOCTYPE html>
<html>
<head>
    <title><?= $title ?></title>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link rel='stylesheet' href='/dpanel/theme/css/style.css' type='text/css'/>
    <link rel='stylesheet' href='/sys/themes/default_full/res/font-awesome.min.css' type='text/css'/>

    <script charset='utf-8' src='/sys/themes/default_full/res/jquery-2.1.1.min.js' type='text/javascript'></script>
    <script charset='utf-8' src='/sys/themes/default_full/res/js.js?1.1' type='text/javascript'></script>
</head>
<body>
<div id="cleaning_block"></div>
<div id="admin_block">
    <ul>
        <a href="#">
            <li class="item_wrap">
                <div class="item_icon fl_l">
                    <span class="fa fa-pencil-square-o"></span>
                </div>
                <div class="item_title">
                    Добавить
                </div>
                <ul>
                    <a href="/">
                        <li>
                            Новость
                        </li>
                    </a>
                    <a href="/">
                        <li>
                            Новость
                        </li>
                    </a>
                </ul>
            </li>
        </a>
        <a href="/">
            <li class="item_wrap">
                <div class="item_icon fl_l">
                    <span class="fa fa-pencil-square-o"></span>
                </div>
                <div class="item_title">
                    Регистрация на сайте
                </div>
            </li>
        </a>
        <a href="/">
            <li class="item_wrap">
                <div class="item_icon fl_l">
                    <span class="fa fa-pencil-square-o"></span>
                </div>
                <div class="item_title">
                    Настройки
                </div>
            </li>
        </a>
    </ul>
    <div style="position: absolute; bottom: 10px; left: 10px; color: #fff;">
        beta 1.0.0
    </div>
</div>
<div id="wrapper" class="clear_fix">
    <header id="header">
        <a href="/"><span class="fa fa-home" aria-hidden="true"></span> На сайт</a>
    </header>

    <div id="title">
        <?= $title ?>
    </div>

    <div id="left_block" class="fl_l">
        <div class="block_content">
            <div class="title">
                Заголовок
            </div>
            <div class="content">
                dasdas
            </div>
        </div>
        <?= $content ?>
    </div>

    <div id="right_block" class="fl_r">
        <div class="block_content">
            <div class="title">
                Заголовок
            </div>
            <div class="content">
                dasdas
            </div>
        </div>
    </div>
</div>

</body>
</html>