<?php foreach($items AS $menu) { ?>
    <?php
    if(isset($menu['group'])) {
        if($menu['group'] > $user->group) {
            continue;
        }
    }
    ?>
    <a href="<?= $menu['url'] ?>">
        <div class="item">
            <?= $menu['title']?>
        </div>
    </a>
<?php } ?>