<?php
/**
 * @var $this document
 */
?><!DOCTYPE html>
<html>
<head>
    <title><?= $title ?> - <?= (isset($_GET['page'])) ? ' Страница ' . $_GET['page'] : '' ?></title>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link rel='stylesheet' href='/sys/themes/default_full/res/style.css?1' type='text/css'/>
    <link rel='stylesheet' href='/sys/themes/default_full/res/font-awesome.min.css' type='text/css'/>
    <noscript>
        <meta http-equiv="refresh" content="0; URL=/bad_browser.html"/>
    </noscript>
    <script>
        (function () {
            var getIeVer = function () {
                var rv = -1; // Return value assumes failure.
                if (navigator.appName === 'Microsoft Internet Explorer') {
                    var ua = navigator.userAgent;
                    var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
                    if (re.exec(ua) !== null)
                        rv = parseFloat(RegExp.$1);
                }
                return rv;
            };
            var ver = getIeVer();
            if (ver !== -1 && ver < 9) {
                window.location.href = "/bad_browser.html";
            }
        })();
    </script>
    <script charset='utf-8' src='/sys/themes/default_full/res/jquery-2.1.1.min.js' type='text/javascript'></script>
    <script charset='utf-8' src='/sys/themes/default_full/res/js.js?1.1' type='text/javascript'></script>
    <meta name="description" content="<?= $description ?> <?= (isset($_GET['page'])) ? ' Страница ' . $_GET['page'] : '' ?>"/>
    <meta name="keywords" content="<?= $keywords ?> <?= (isset($_GET['page'])) ? ' Страница ' . $_GET['page'] : '' ?>"/>
    <?php
    if($meta_og) {
        echo '<meta property="og:type" content="article" />' . "\n";
        foreach ($meta_og as $k => $v) {
            echo '<meta property="og:'.$k.'" content="'.$v.'" />' . "\n";
        }
    }
    ?>
</head>
<body>
    <div id="cleaning_block"></div>
    <div id="admin_block">
        <div id="admin_bars" class="item_wrap" style="margin-bottom: 30px">
            <div class="item_icon fl_l">
                <span class="fa fa-bars"></span>
            </div>
            <div class="item_title">
                Текст еще больше тут
            </div>
        </div>
        <div class="item_wrap">
            <div class="item_icon fl_l">
                <span class="fa fa-pencil-square-o"></span>
            </div>
            <div class="item_title">
                Текст еще больше тут
            </div>
        </div>
        <div class="item_wrap">
            <div class="item_icon fl_l">
                <span class="fa fa-pencil-square-o"></span>
            </div>
            <div class="item_title">
                Текст
            </div>
        </div>
    </div>

    <div id="wrapper">
        <header id="header">
            <div id="logo">
                <img src="/sys/themes/default_full/img/logo.png" alt="" />
            </div>
            <h1 id="logo_info">
                ФК "АВАНГАРД"
                <div style="font-size: 18px; padding-left: 20px">
                    Официальный сайт
                </div>
            </h1>
            <div id="head_time">
                Сгодня <?= date("j", TIME) . " " . Misc::getLocaleMonth(date("n", TIME)) . " " . date("Y", TIME) ?>
            </div>
            <div id="navigation">
                <ul>
                    <a href="/">
                        <li class="item fl_l">
                            Главная
                        </li>
                    </a>
                    <li class="item fl_l">
                        Клуб
                    </li>
                    <li class="item fl_l">
                        Команда <span class="fa fa-arrow-circle-down"></span>
                        <ul>
                            <li>lol</li>
                            <li>lol</li>
                        </ul>
                    </li>
                    <li class="item fl_l">
                        Чето там еще
                    </li>
                    <li class="item fl_l">
                        О клубе <span class="fa fa-arrow-circle-down"></span>
                        <ul>
                            <li>Тут очень важное</li>
                            <li>И тут както так вот</li>
                            <li>Тут очень важное</li>
                            <li>И тут както так вот</li>
                        </ul>
                    </li>
                    <li class="item fl_l">
                        Чето там еще
                    </li>
                </ul>
            </div>
        </header>

        <div id="content" class="clear_fix">
            <div id="left_content" class="fl_l">
                <?= $content ?>
            </div>

            <div id="right_content" class="fl_r">
                <?= Menu::section("previous_match"); ?>
                <?= Menu::section("next_match"); ?>
            </div>
        </div>
    </div>
    <footer id="footer">
        <div id="width_limit">
            <?= $document_generation_time ?>
        </div>
    </footer>
</body>
</html>