<aside class="menu_wrapper">
    <div class="title">
        Заголовок
    </div>
    <div class="content">
        <?php foreach($items AS $menu) { ?>
            <a href="<?= $menu['url'] ?>">
                <div class="item">
                    <?= $menu['title'] ?>
                </div>
            </a>
        <?php } ?>
    </div>
</aside>