<aside class="menu_wrapper">
    <div class="menu_title">
        <?= $title ?>
    </div>
    <div class="match_wrapper">
        <div class="match_info">
            <?= $items['text_top'] ?>
        </div>
        <div class="match_table">
            <div class="left_logo">
                <img src="<?= $items['logo1'] ?>" alt="" />
                <?= $items['command1'] ?>
            </div>
            <div class="center_score">
                <?= $items['score'] ?>
            </div>
            <div class="right_logo">
                <img src="<?= $items['logo2'] ?>" alt="" />
                <?= $items['command2'] ?>
            </div>
        </div>
        <div class="match_info">
            <?= $items['text_bot'] ?>
        </div>
    </div>
</aside>