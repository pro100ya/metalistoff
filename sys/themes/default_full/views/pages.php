<div <?= ($id)? 'id="'.$id.'"' : '' ?> class="pages">
    <?php
    if($page == 1) {
        echo '<span class="nolinks">1</span>';
    } else {
        echo '<a class="links" '.((!$remove_href) ? 'href="' . $link . 'page=1"' : "").'>1</a>';
    }
    for ($i = max(2, $page - $st); $i < min($k_page, $page + $st + 2); $i++) {
        if ($i == $page)
            echo '<span class="nolinks">' . $i . '</span>';
        else
            echo '<a class="links" '.((!$remove_href) ? 'href="' . $link . 'page=' . $i . '"' : "").'>' . $i . '</a>';
    }
    if($page == $k_page) {
        echo '<span class="nolinks">' . $k_page . '</span>';
    } else {
        echo '<a class="links" '.((!$remove_href) ? 'href="' . $link . 'page=' . $k_page . '"' : "").'>' . $k_page . '</a>';
    }
    ?>
</div>
