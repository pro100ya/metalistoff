<div class="news_wrapper">
    <div class="img_wrapper">
        <?php if($off_news) { ?>
            <div class="off_news" data-ttip="Официально">
                <div class="icons check"></div>
            </div>
        <?php } ?>
        <div class="counters">
            Просмотров: <?= $views ?>
            <div class="fl_r">
                Комментариев: <?= $comments ?>
            </div>
        </div>
        <div class="title">
            <a href="<?= $url ?>">
                <?= $title ?>
            </a>
        </div>
        <div class="img">
            <img src="<?= $img ?>" alt="" />
        </div>
    </div>
    <div class="text">
        <?= $text ?>
    </div>
</div>