<div class="news_view">
    <div class="block_info clear_fix">
        <div class="fl_l">
            Просмотров: <?= $views ?>
        </div>
        <div class="fl_r">
            Добавлено: <?= $time ?>
        </div>
    </div>
    <?php if(isset($img)) { ?>
        <img class="center" src="<?= $img ?>" alt="<?= $title ?>" />
    <?php } ?>
    <div class="block_title">
        <?= $title ?>
    </div>
    <div class="block_text">
        <?= $text ?>
    </div>

    <div class="block_tags">
        Теги: <?= $tags ?>
    </div>

    <div class="recommended">
        <div class="title">
            Рекомендовано
        </div>
        <div class="items_wrap clear_fix">
            <?php foreach ($more_news AS $news) { ?>
                <div class="item fl_l">
                    <div class="img_wrap">
                        <img class="center" src="<?= $news['img'] ?>" alt="<?= $news['title'] ?>" />
                    </div>
                    <a href="<?= $news['url'] ?>"><?= $news['title'] ?></a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>