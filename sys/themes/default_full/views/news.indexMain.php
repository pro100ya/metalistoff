<div id="block_news">
    <div class="block_news_title">
        Новости
    </div>
    <div class="items_wrap clear_fix">
        <?php foreach ($main_news AS $news) { ?>
            <div class="item fl_l">
                <div class="img_wrap">
                    <img src="/1.jpg" alt="" />
                </div>
                <div class="item_title">
                    <a href="<?= $news['url'] ?>"><?= $news['title'] ?></a>
                </div>
                <div class="item_info">
                    <span class="fa fa-clock-o"></span>  <?= $news['time'] ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>