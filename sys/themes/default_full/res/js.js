/**
 * Created by ZeroTwink on 13.03.2016.
 */

$(document).ready(function() {
    /**
     * При смене файла в input file
     */
    $(document).on('change', '.upload', function() {
        var par = $(this).parent();
        if(this.value) {
            par.find(".textfile").html("Файл готов к загрузке");
            par.find(".selectbutton").css({"background-color": "#73BF2B"});
        } else {
            par.find(".textfile").html("Файл не выбран")
            par.find(".selectbutton").css({"background-color": "#A2A3A3"});
        }
    });


    /**
     * Слайдер новостей в меню
     */
    $(".slide_program_bottom").on('click', function() {
        var myThis = $(this);
        var parentSlideImg = myThis.parent().find(".slide_program_img");
        console.log(parentSlideImg.css('display'));
        if(myThis.parent().find(".slide_program_img").css('display') === 'block') {
            return;
        }

        $(".slide_program_wrap .slide_program_bottom").removeClass("slide_program_bottom_act");

        myThis.addClass("slide_program_bottom_act");

        $('.slide_program_img').slideUp(300);
        parentSlideImg.slideDown(300);
        //myThis.removeClass('slide_program_bottom');
    });



    
    
    
    
    
    $("#admin_bars").on("click", function (event) {
        if($("#admin_block").width() > 50) {
            $("#admin_block").width(50);
        } else {
            $("#admin_block").width(200);
        }
    });
    
    








    //$(".textarea_animate").autosize();











    var Ttip = {
        isTitle: '',
        isDataTitle: false,
        ttipOver: function (myThis) {
            var stopDie = true;
            if (myThis.attr("title")) {
                stopDie = false;
            }
            if (myThis.is("[data-ttip]")) {
                stopDie = false;
                Ttip.isDataTitle = true;
            }

            if(stopDie) {
                return;
            }

            if(!Ttip.isDataTitle) {
                Ttip.isTitle = myThis.attr("title");
                myThis.attr("title", "");
            } else {
                Ttip.isTitle = myThis.attr("data-ttip");
            }

            var html = '<div class="ttip">'+Ttip.isTitle+'</div>';

            $("#cleaning_block").append(html);

            var ttip = $(".ttip");

            if(ttip.length > 1) {
                ttip.filter(":first").remove();
            }

            ttip.stop(true);
            ttip.delay(300).fadeIn(300);

            ttip.css({
                top: myThis.offset().top + myThis.outerHeight() + 10,
                left: myThis.offset().left
            });
        },
        ttipOut: function (myThis) {
            var ttip = $(".ttip");

            ttip.stop(true);
            ttip.fadeOut(300, function() {
                ttip.remove();
            });

            if(!Ttip.isDataTitle) {
                myThis.attr("title", Ttip.isTitle);
            }

            Ttip.isTitle = '';
            Ttip.isDataTitle = false;
        }
    };

    $(document).on('mouseover mouseout', '[title], [data-ttip]', function(event) {
        var myThis = $(this);

        if(event.type === 'mouseover') {
            Ttip.ttipOver(myThis);
            return;
        }

        if(event.type === 'mouseout') {
            Ttip.ttipOut(myThis);
            return;
        }
    });












    $("#i_news_wrapper .i_news_item").on('mouseenter mouseleave', function() {
        var myThis = $(this);
        if(myThis.find(".counters").css("display") === "none") {
            myThis.find(".counters").slideDown(100);
        } else {
            myThis.find(".counters").slideUp(100);
        }
    });










    //var newsWrapper = $("#i_news_wrapper").height();
    //var stopScroll = false;
    //$(window).scroll(function () {
    //    if ($(this).scrollTop() > $("#i_table_left").height() + 900) {
    //        if(stopScroll === false) {
    //            $("#i_table_left").hide();
    //            $("#i_table_right").hide();
    //            $("#i_table_center").css({width: '100%', "margin-left": "0px"});
    //
    //            $("#i_news_wrapper").css({width: '100%', "padding-top": $("#i_table_left").height() - 400 + "px"});
    //
    //            $("#i_news_wrapper .i_news_item").css({"display": "inline-block", width: '486px'});
    //
    //            stopScroll = true;
    //        }
    //    }
    //    else {
    //        $("#i_table_left").show();
    //        $("#i_table_right").show();
    //        $("#i_table_center").css({width: '470px', "margin-left": "10px"});
    //        $("#i_news_wrapper").css({width: '470px'});
    //
    //        $("#i_news_wrapper .i_news_item").css({"display": "block", width: '448px'});
    //
    //        $("#i_news_wrapper").css({width: '100%', "padding-top": "10px"});
    //
    //        stopScroll = false;
    //    }
    //});











    $("#user_menu_slide").on('click', function() {
        var menuWrapper = $("#user_menu_wrapper");
        var menuButton = $("#user_menu_slide");
        if(menuWrapper.css("display") === 'none') {
            menuWrapper.slideDown(300);
            menuButton.css({"background-color": "#424242"});
        } else {
            menuWrapper.slideUp(300);
            menuButton.css("background-color", "");
        }
    });















    $("#i_items_wrapper .i_item").on('click', function() {
        var myThis = $(this);

        $("#i_items_wrapper .i_item .img").removeClass("img_action");

        myThis.find(".img").addClass("img_action");

        $("#i_content_wrapper #i_img_wrapper img").attr("src", myThis.attr("data-img"));
        $("#i_content_wrapper").find("#i_title").html('<a href="'+myThis.attr("data-url")+'">'+myThis.attr("data-title")+'</a>');
        $("#i_content_wrapper").find("#i_text").html(myThis.attr("data-text"));
    });















    $("#i_news_lenta_wrapper").mCustomScrollbar({
        axis:"y",
        theme:"minimal-dark"
    });















    $(".myselect_wrapper").on("click", function(event) {
        var myThis = $(this);
        var myselectDropdown = myThis.find(".myselect_dropdown");

        $(".myselect_dropdown").slideUp(100);

        if(myselectDropdown.css("display") == "block") {
            myselectDropdown.slideUp(100);
            return;
        }
        myselectDropdown.slideDown(100);
    });

    $(".myselect_dropdown").on('click', 'li', function(event) {
        event.stopPropagation();

        var myThis = $(this);

        myThis.parent().find("input").val(myThis.attr("data-value"));

        myThis.parent().parent().parent().find(".myselect_select_text").html(myThis.html());

        $(".myselect_dropdown").slideUp(100);
    });

    $(document).on('click', function(event) {
        if (!$(event.target).closest(".myselect_wrapper").length) {
            $(".myselect_dropdown").slideUp(100);
        }
    });














    /**
     * Открытие/Скрытие спойлера
     */
    $("#wrapper").on('click', '.spoiler_title', function() {
        $(this).parent().find(".spoiler_content:first").slideToggle(100);
    });












    /**
     * Вставка бб-кодов в текстове поле
     */
    function addOnTextarea(textarea, open, close) {
        var myValue = open + close;

        if (document.selection) {
            textarea.focus();
            document.selection.createRange().text = myValue;
        }
        else if (textarea.selectionStart || textarea.selectionStart == '0') {
            var position = textarea.selectionStart;
            textarea.value = textarea.value.substring(0,textarea.selectionStart) +
                myValue + textarea.value.substring(textarea.selectionEnd,textarea.value.length);
            textarea.selectionStart = textarea.selectionEnd = position  + open.length;
        } else {
            textarea.value += myValue;
        }
        textarea.focus();
    }

    $("#wrapper").on('click', '.redactor_toolbar span', function(event) {
        event.stopPropagation();

        var mThis = $(this);

        var textarea = this.parentNode.parentNode.parentNode.children[1];
        var teg = mThis.attr('data-teg');

        var open = '[' + teg + ']';
        var close = '[/' + teg + ']';

        if(teg === 'spoiler') {
            open = '[' + teg + '=Скрытый текст]';
            close = '[/' + teg + ']';
        }
        if(teg === 'url') {
            var html = '<div style="text-align: center" class="form">\n\
                            Название ссылки <br />\n\
                            <input id="name_url" name="name" class="form_control" type="text" value="Ссылка" /><br />\n\
                            Сылка: пример: http://lastanime.ru <br />\n\
                            <input id="url" name="url" class="form_control" type="text" placeholder="http://" /><br />\n\
                            <div style="text-align: center"><input id="save_url" type="submit" value="Закрепить" /></div>\n\
                        </div>';
            contentModal.modal(html, 'Вставка ссылки', {width: 300});

            $("#save_url").on('click', function(event) {
                event.stopPropagation();
                event.preventDefault();

                var nameUrl = $("#name_url").val();
                var url = $("#url").val();

                if(!nameUrl || !url) {
                    MessagesModal.modalError('Необходимо заполнить все поля');
                    return;
                }

                open = '[url='+url+']'+nameUrl+'[/url]';
                close = '';

                addOnTextarea(textarea, open, close);

                contentModal.close();

                $("#save_url").off('click');
            });

            return;
        }

        if(teg === 'img') {
            var html = '<div style="text-align: center" class="form">\n\
                            Ссылка на изображение <br />\n\
                            <input id="img_url" name="name" class="form_control" type="text" /><br />\n\
                            <div style="text-align: center"><input id="save_img" type="submit" value="Закрепить" /></div>\n\
                        </div>';
            contentModal.modal(html, 'Вставка изображения', {width: 300});

            $("#save_img").on('click', function(event) {
                event.stopPropagation();

                var imgUrl = $("#img_url").val();

                if(!imgUrl) {
                    MessagesModal.modalError('Необходимо заполнить поле');
                    return;
                }

                open = '[img]'+imgUrl+'[/img]';
                close = '';

                addOnTextarea(textarea, open, close);

                contentModal.close();

                $("#save_img").off('click');
            });

            return;
        }

        if(teg === 'smiles') {
            var emojiModal = $("#emoji_modal");

            if(emojiModal.css('display') == 'none') {
                emojiModal.find('.emoji_list').on('click', function(event) {
                    open = $(this).attr('data-emoji');
                    close = '';
                    addOnTextarea(textarea, open, close)
                });
                emojiModal.css({
                    left: mThis.position().left - 240,
                    top: mThis.position().top - 200
                }).fadeIn(300);
            } else {
                emojiModal.find('.emoji_list').off('click');
                emojiModal.fadeOut(300);
            }

            return;
        }

        if(teg === 'video') {
            var html = 'Выберите сайт с которого хотите добавить видео<br />\n\
                <div class="bb_video" data-from="youtube">Youtube</div>\n\
                <div class="bb_video" data-from="sibnet">Sibnet</div>\n\
                <div class="bb_video" data-from="vk">VK</div>';

            contentModal.modal(html, 'Вставка видео', {width: 400});

            var from = '';

            $(".bb_video").on('click', function(event) {
                event.stopPropagation();
                console.log(event);

                from = $(this).attr("data-from");

                var html2 = '<div style="text-align: center" class="form">\n\
                    Ссылка на видео '+from+'<br />\n\
                    <input id="video_url" name="name" class="form_control" type="text" /><br />\n\
                    <div style="text-align: center"><input id="save_video" type="submit" value="Закрепить" /></div>\n\
                    <div class="myhr"></div><br />\n\
                    ПРИМЕР<br />\n\
                    <img src="/sys/images/info_add_video/'+from+'.png" alt="" />\n\
                    </div>';

                $(".content_modal_content").html(html2);

                $("#save_video").on('click', function(event) {
                    var url = $("#video_url").val();
                    if(!url) {
                        alert('Ссылка на видео обязательна');

                        return;
                    }
                    open = '[video='+from+']'+url+'[/video]';
                    close = '';

                    addOnTextarea(textarea, open, close);

                    contentModal.close();
                });
            });

            return;
        }

        if(open && close) {
            addOnTextarea(textarea, open, close)
        }
    });
    /*
     * END втавка бб кодов
     */
});
















var contentModal = {
    win: $(window),
    doc: $(document),
    modal: function(html, title, settings) {
        var set = {
            height: 0,
            width: 0,
            stopClose: false /* Закрывать при клике в не областе модального окна */
        };

        set = $.extend(set, settings);

        var bodyHtml =
            '<div class="modales_mask"></div>\n\
            <div class="content_modal_wrap">\n\
                <div class="content_modal">\n\
                    <div class="content_modal_header">\n\
                        <div class="fl_r content_modal_close">Закрыть</div>\n\
                        '+title+'\n\
                    </div>\n\
                    <div class="content_modal_content">\n\
                        '+html+'\n\
                    </div>\n\
                </div>\n\
            </div>';

        $("#cleaning_block").prepend(bodyHtml);

        var bodyWidth = contentModal.win.width();
        $("body").css({overflow: "hidden", width: bodyWidth});

        var main = $(".content_modal");

        /*
         * Сразу ставим ширину чтобы высота вычислялась уже относительно ширины
         */
        main.css({width: set.width});


        if(set.height <= 0) {
            set.height = main.height();
        }
        if(!set.width) {
            set.width = main.width();
        }

        if(set.height > contentModal.win.height()) {
            var top = 20;
        } else {
            var top = ((contentModal.win.height() / 2) - (set.height / 2));
        }

        main.css({
            top: top,
            left: (contentModal.win.width() / 2) - (set.width / 2)
        });

        main.hide().fadeIn(500);

        $(".content_modal_close").on('click', function(){
            contentModal.close();
        });

        if(!set.stopClose) {
            contentModal.doc.on('click.contentMod', function(event) {
                if ($(".content_modal").has(event.target).length === 0) {
                    contentModal.close();
                }
            });
        }
    },
    close: function() {
        contentModal.doc.off('click.contentMod');
        $(".content_modal_wrap").remove();

        var mask = $(".modales_mask");

        $("body").css({overflow: "", width: ""});

        mask.fadeOut(300, function() {
            mask.remove();
        });
    }
};












var Profile = {
    avatarNew: function(btn, event) {
        event.stopPropagation();
        event.preventDefault();

        $.ajax({
            type: "POST",
            url: '/profile/ajax.avatar.php',
            data: {
                "form": 1
            },
            dataType: "json",
            cache: false,
            success: function(r) {
                contentModal.modal(r.html, 'Смена аватара', {width: 400});

                $(".content_modal_content").on('submit', '.avatar_new', function(event) {

                    event.preventDefault();

                    var data = new FormData();

                    data.append(this.elements[0].name, this.elements[0].files[0]);
                    data.append('photo', 1);

                    $.ajax({
                        type: "POST",
                        url: '/profile/ajax.avatar.php',
                        data: data,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        cache: false,
                        success: function(r) {
                            contentModal.close();

                            Profile.avatarResize(r.img);
                        },
                        error: function(r) {
                            console.log(r);
                        }
                    });
                });
            }
        });
    },
    avatarResize: function(path_img, event) {
        event = event || window.event;
        event.stopPropagation();
        var img = new Image();
        img.src = path_img + '?' + Math.random();
        img.onload = function(event) {
            var html = '<img id="target" src="'+img.src+'" alt="" />';
            html += '<div id="crop" style="padding: 20px; \n\
                background-color: #CBDAEA; text-align: center; font-weight: \n\
                bold; margin: 10px 0 4px 0; cursor: pointer;">Применить изменения</div>';
            contentModal.modal(html, 'Смена аватара', {
                width: img.width + 20,
                stopClose: true
            });

            var x1, y1, x2, y2;
            var  jcrop_api;

            $('#target').Jcrop({
                onChange: showCoords,
                onSelect: showCoords,
                minSize: [200, 200],
                maxSize:  [600, 600],
                setSelect: [0, 0, 200, 200],
                bgOpacity: .3,
                aspectRatio: 1
            },function(){
                jcrop_api = this;
            });
            // Снять выделение
            $('#release').click(function(e) {
                release();
            });
            // Изменение координат
            function showCoords(c){

                //if(c.w > c.h) {
                //    jcrop_api.setOptions({setSelect: [c.x, c.y, c.h, c.h]});
                //}

                x1 = c.x;
                y1 = c.y;
                x2 = c.x2;
                y2 = c.y2;
            }

            $('#crop').click(function(e) {
                $.ajax({
                    type: "POST",
                    url: '/profile/ajax.avatar.php',
                    data: {'x1': x1, 'x2': x2, 'y1': y1, 'y2': y2, resize: 1},
                    dataType: "json",
                    cache: false,
                    success: function(r) {
                        jcrop_api.setOptions({setSelect: [0, 0, 200, 200]});
                        $("#profile_image img").attr('src', r.img);
                        contentModal.close();
                    }
                });
            });
        }
    }
};














var imgModal = {
    modal: function(img, settings) {
        var image = new Image();
        image.src = img;


        image.onload = function(event) {
            loading();
        }

        function loading() {
            if(!image.width || image.width <=0) {
                return;
            }

            var html =
                '<div class="img_modal">\n\
                    <div class="img_modal_close">Закрыть</div>\n\
                    <div class="img_modal_content"><img src="'+img+'" alt="" /></div>\n\
                </div>';

            $("#cleaning_block").prepend(html);
            var width = image.width + 20,
                height = image.height + 20;


            $(".img_modal").hide();

            var isTop;
            if(height + 20 > $(window).height()) {
                isTop = $(document).scrollTop() + 30;
            } else {
                isTop = (($(window).height() / 2) - (height / 2)) + $(document).scrollTop();
            }


            $(".img_modal").css({
                top: isTop,
                left: ($(window).width() / 2) - (width / 2)
            });

            $(".img_modal").fadeIn(300);


            $(".img_modal_close").on('click', function(){
                imgModal.close();
            });

            $(document).on('click.modalImg', function(event) {
                imgModal.close();
                $(document).off('click.modalImg');
            });
        }
    },
    close: function() {
        $(".img_modal").remove();
    }
};

$(document).ready(function() {
    $("#wrapper").on('click', '.viewimg', function() {
        var img = '';
        if($(this).attr("data-photo")) {
            img = $(this).attr("data-photo");
        } else {
            img = this.src;
        }
        imgModal.modal(img);


    });
});
