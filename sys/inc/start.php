<?php

// Проверяем версию PHP
version_compare(PHP_VERSION, '5.4', '>=') or die('Требуется PHP >= 5.4');

/**
 * Константы и функции, необходимые для работы движка.
 * Выделены в отдельный файл чтобы избежать дублирования кода в инсталляторе
 */
require_once dirname(__FILE__) . '/initialization.php';

/*
 * Подгруска файла настрок
 */
$sys = SysConfig::getInstance();


if (!empty($_SESSION [SESSION_ID_USER])) {
    $user = new User($_SESSION [SESSION_ID_USER]);
} elseif (!empty($_COOKIE[COOKIE_ID_USER]) && !empty($_COOKIE[COOKIE_USER_PASSWORD])) {
    $tmp_user = new User($_COOKIE[COOKIE_ID_USER]);

    if (Crypt::hash(Crypt::decrypt($_COOKIE[COOKIE_USER_PASSWORD])) === $tmp_user->password) {
        $user = $tmp_user;

        $_SESSION[SESSION_ID_USER] = $user->id;
    }
} else {
    $user = new User();
}

/**
 * обработка данных пользователя
 */
if ($user->id !== false) {
    //$user->last_visit = TIME;
    //$user->activity += 0.001;

    $time_last = TIME - SESSION_LIFE_TIME;
    $res = Db::me()->prepare("DELETE FROM `users_online` WHERE `time_last` < ?");
    $res->execute(Array($time_last));

    $q = Db::me()->prepare("SELECT * FROM `users_online` WHERE `id_user` = ? LIMIT 1");
    $q->execute(Array($user->id));

    if ($q->fetch()) {
        $res = Db::me()->query("UPDATE `users_online` SET `time_last` = '".TIME."' WHERE `id_user` = ".$user->id." LIMIT 1");
    } else {
        $res = Db::me()->prepare("INSERT INTO `users_online` (`id_user`, `time_last`) VALUES (?, ?)");
        $res->execute(Array($user->id, TIME));
    }
}



    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', true);
