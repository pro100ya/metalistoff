<?php
include_once 'sys/inc/start.php';
$doc = new Document();

$doc->title = 'Регистрация';

if($user->id) {
    $doc->accessDenied('Произошла ошибка Вы уже вошли на сайт как пользователь');
}

if(isset($_GET['ref'])) {
    setcookie('ref', (int)$_GET['ref'], TIME + 60 * 60 * 24);
}

if(isset($_POST['save'])) {
    if(!$_POST['login']) {
        $doc->err('Введите логин');
        exit();
    } elseif (!$_POST['password']) {
        $doc->err('Введите пароль');
        exit();
    }
    
    if($_POST['login'] != htmlspecialchars($_POST['login'])) {
        $doc->err('В нике есть запрешенные символы');
        exit();
    }
    
    if (!Valid::nick($_POST['login'])) {
        $doc->err('Недоступный ник');
        exit();
    }
    
    if (!Valid::password($_POST['password'])) {
        $doc->err('Не корректный пароль');
        exit();
    }
    
    if($_POST['password'] != $_POST['password2']) {
        $doc->err('Пароли не сошлись');
        exit();
    }
    
    $q = Db::me()->prepare("SELECT `login` FROM `users` WHERE `login` = ? LIMIT 1");
    $q->execute(Array($_POST['login']));
    if ($q->fetch()) {
        $doc->err('Логин занят');
        exit();
    }
    $res = Db::me()->prepare("INSERT INTO `users` (`login`, `password`, `sex`, `date_reg`) VALUES (?, ?, ?, ?)");
    $res->execute(Array($_POST['login'], Crypt::hash($_POST['password']), $_POST['sex'], TIME));
    $id_user = Db::me()->lastInsertId();
    
    $_SESSION[SESSION_ID_USER] = $id_user;
    setcookie(COOKIE_ID_USER, $id_user, TIME + 60 * 60 * 24 * 365);
    setcookie(COOKIE_USER_PASSWORD, Crypt::encrypt($_POST['password']),TIME + 60 * 60 * 24 * 365);
    
    $doc->msg('Регистрация прошла успешно');
    header("Location: /profile.php");
    exit();
}

$form = new Form();
$form->input("login", "Ваш логин на сайте");
$form->password("password", "Ваш пароль");
$form->password("password2", "Повторите пароль");

$options = array();
$options[] = array(1, 'Мужской');
$options[] = array(0, 'Женский');
$form->select("sex", "Ваш пол", $options);

$form->button("Зарегистрироваться", "save");
$form->display();