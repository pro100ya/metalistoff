<?php
include_once '../sys/inc/start.php';
$doc = new Document(2);
$doc->title = "Добавить новость";

$doc->ret("Админка", "/dpanel/");

if(isset($_POST['save'])) {
    $title = $_POST['title'];
    $type = (int) $_POST['type'];
    $text = $_POST['text'];
    $tags = $_POST['tags'];
    $source_name = $_POST['source_name'];
    $source_link = $_POST['source_link'];
    $i_bold = (int) $_POST['i_bold'];
    $i_photo = (int) $_POST['i_photo'];
    $i_video = (int) $_POST['i_video'];
    $off_news = (int) $_POST['off_news'];

    $in_news = Db::me()->prepare("INSERT INTO `news` (`title`, `id_user`, `type`, `text`, `tags`, `source_name`, `source_link`, `i_bold`, `i_photo`, `i_video`, `off_news`, `time`)
              VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $in_news->execute(Array($title, $user->id, $type, $text, $tags, $source_name, $source_link, $i_bold, $i_photo, $i_video, $off_news, TIME));

    $last_id = Db::me()->lastInsertId();

    if(isset($_FILES['img']) && $_FILES['img']['size'] > 10) {

        $dir = new Files(H.'/sys/tmp/');
        $dir->setAllowedType(array('jpeg','jpg','png','gif'));

        if($dir->typeChecking($_FILES ['img']['name'])) {
            $typef = $dir->typeFile($_FILES ['img']['name']);
            $namef = 'post_' . $last_id . '.' . $typef;

            if(!$rtr = $dir->upload(array($_FILES['img']['tmp_name'] => $namef))) {
                $doc->err('При загрузки файла ошибка');
                $err = true;
            }

            $folder = substr($last_id, -1);

            $scr = new ImageResize(H.'/sys/tmp/' . $namef);
            $scr->resizeToWidth(492);
            $scr->saveImage(H."/sys/files/news/original/folder".$folder."/".'post_' . $last_id.".jpg" , 90);

            $scr = new ImageResize(H."/sys/files/news/original/folder".$folder."/".'post_' . $last_id.".jpg");
            $scr->resizeToWidth(246);
            $scr->saveImage(H."/sys/files/news/medium/folder".$folder."/".'post_' . $last_id.".jpg" , 90);

            $scr = new ImageResize(H."/sys/files/news/medium/folder".$folder."/".'post_' . $last_id.".jpg");
            $scr->resizeToWidth(160);
            $scr->saveImage(H."/sys/files/news/mini/folder".$folder."/".'post_' . $last_id.".jpg" , 90);



            $img = "folder".$folder."/".'post_' . $last_id.".jpg";

            unlink(H.'/sys/tmp/' . $namef);

            if(!isset($err)) {
                $res = Db::me()->query("UPDATE `news` SET `img` = '$img' WHERE `id` = $last_id");
            }
        } else {
            $err = true;
            $doc->err('Файл не является изоброжением или тип файла не доступен');
        }
    }

    $sitemap = new Sitemap();
    $sitemap->createMap();
    $sitemap->saveMapXml();

    if(!isset($err)) {
        header("Location: /news/" . Text::urlToString($last_id . "-" . $title));
    }
}


$form = new Form();

$options = array();
$options[] = array(0, 'Главные новости');
$options[] = array(1, 'Лента новостей');
$form->select("type", "Тип новости (Главная, Лента)", $options);

$form->input("title", "Заголовок");

$form->textarea("text", "Содержание новости");

$form->input("source_name", "Название Источник");
$form->input("source_link", "Ссылка Источник");

$form->input("tags", "Теги (Футбол,Трансферы,Франция)");

$form->file('img', "Постер min(680x408px)");

$options = array();
$options[] = array(0, 'Не выделять');
$options[] = array(1, 'Да выделить');
$form->select("i_bold", "Выделить жирным (-2 балла)", $options);

$options = array();
$options[] = array(0, 'Нет фотографий');
$options[] = array(1, 'Да есть фотографии');
$form->select("i_photo", "Присутствует Фото (-2 балла)", $options);

$options = array();
$options[] = array(0, 'Нет видео');
$options[] = array(1, 'Да есть видео');
$form->select("i_video", "Присутствует Видео (-2 балла)", $options);

$options = array();
$options[] = array(0, 'Нет');
$options[] = array(1, 'Опубликовать');
$form->select("inguest", "Опубликовать в гостевой (-2 балла)", $options);

$form->checkbox("off_news", "Официальная новость");

$form->button("Опубликовать", "save");
$form->display();