<?php
include_once '../sys/inc/start.php';
$doc = new Document(2);
$doc->title = "Добавить новость сайта";

$doc->ret("Админка", "/dpanel/");

if(isset($_POST['save'])) {
    $title = $_POST['title'];
    $text = $_POST['text'];

    $in_news = Db::me()->prepare("INSERT INTO `news_site` (`title`, `id_user`, `text`, `time`) VALUES (?, ?, ?, ?)");
    $in_news->execute(Array($title, $user->id, $text, TIME));

    $last_id = Db::me()->lastInsertId();



//    if(!isset($err)) {
//        header("Location: /news/" . Text::urlToString($last_id . "-" . $title));
//    }
}


$form = new Form();
$form->input("title", "Заголовок");

$form->textarea("text", "Содержание новости");

$form->button("Опубликовать", "save");
$form->display();