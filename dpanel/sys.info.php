<?php
include_once '../sys/inc/start.php';
$doc = new Document(4);
$doc->title = 'Админка';

// проверка версии PHP
if (version_compare(PHP_VERSION, '5.3', '>=')) {
    echo 'PHP >= 5.3: ОК (' . PHP_VERSION . ') <br /><br />';
} else {
    echo 'Требуется PHP >= %s (сейчас %s)', '5.3<br /><br />';
}
// проверка MySQL
if (function_exists('mysql_info')) {
    echo 'MySQL: OK<br /><br />';
} else {
    echo 'Невозможно получить информацию о MySQL<br /><br />';
}

// проверка PDO
if (class_exists('pdo')) {
    if (array_search('mysql', PDO::getAvailableDrivers()) !== false) {
        echo 'PDO: OK<br /><br />';
    } else {
        echo 'Нет драйвера mysql для PDO<br /><br />';
    }
} else {
    echo 'Необходимо подключить PDO<br /><br />';
}

// шифрование
if (function_exists('mcrypt_module_open')) {
    echo 'mcrypt: OK<br /><br />';
} else {
    echo 'Отсутствие mcrypt не позволит шифровать COOKIE пользователя.<br /><br />';
}

// работа с графикой
if (function_exists('gd_info')) {
    echo 'GD: OK<br /><br />';
} else {
    echo 'Нет библиотеки GD<br /><br />';
}

// снятие ограничения по времени выполнения скрипта
if (function_exists('set_time_limit')) {
    echo 'set_time_limit: OK<br /><br />';
} else {
    echo 'Функция set_time_limit() не доступна. Могут возникнуть проблемы при обработке ресурсоемких задач.<br /><br />';
}  // функции для работы с UTF
if (function_exists('mb_internal_encoding') && function_exists('iconv')) {
    echo 'mbstring и Iconv: OK<br /><br />';
} elseif (!function_exists('mb_internal_encoding') && !function_exists('iconv')) {
    echo 'Необходим по крайней мере один из модулей: mbstring или Iconv<br /><br />';
} elseif (function_exists('mb_internal_encoding')) {
    echo 'mbstring: OK<br /><br />';
} elseif (function_exists('iconv')) {
    echo 'Iconv: OK<br /><br />';
}
// обработка видео (снятие скриншотов)
if (class_exists('ffmpeg_movie')) {
    echo 'FFmpeg: OK<br /><br />';
} else {
    echo'Без FFmpeg автоматическое создание скриншотов к видео недоступно<br /><br />';
}

