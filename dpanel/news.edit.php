<?php
include_once '../sys/inc/start.php';
$doc = new Document(2);
$doc->title = 'Редактировать новость';

$doc->ret("Админка", "/dpanel/");

if(!isset($_GET['id']) && !isset($_POST['id'])) {
    $form = new Form();
    $form->input("id", "Индификатор новости которую редактировать");
    $form->button("Отправить", "send_id");
    $form->display();

    exit();
}

if(isset($_POST['id'])) {
    header("Location: /dpanel/news.edit.php?id=" . $_POST['id']);
}


if(!isset($_GET['id'])) {
    $doc->accessDenied("Обращение к несуществующей новости");
}

$id = (int)$_GET['id'];

$res = Db::me()->prepare("SELECT * FROM `news` WHERE `id` = :id LIMIT 1");
$res->execute(array(':id' => $id));

$news = $res->fetch();

if(!$news) {
    $doc->accessDenied("Обращение к несуществующей новости");
}


if(isset($_POST['save'])) {
    $news['title'] = $title = $_POST['title'];
    $news['type'] = $type = (int) $_POST['type'];
    $news['text'] = $text = $_POST['text'];
    $news['tags'] = $tags = $_POST['tags'];
    $news['source_name'] = $source_name = $_POST['source_name'];
    $news['source_link'] = $source_link = $_POST['source_link'];
    $news['i_bold'] = $i_bold = (int) $_POST['i_bold'];
    $news['i_photo'] = $i_photo = (int) $_POST['i_photo'];
    $news['i_video'] = $i_video = (int) $_POST['i_video'];
    $news['off_news'] =  $off_news = (int) $_POST['off_news'];

    $up_news = Db::me()->prepare("UPDATE `news` SET `title` = ?, `id_user` = ?,
                `type` = ?, `text` = ?, `tags` = ?, `source_name` = ?,
                `source_link` = ?, `i_bold` = ?, `i_photo` = ?, `i_video` = ?, `off_news` = ? WHERE `id` = ? LIMIT 1");
    $up_news->execute(Array($title, $user->id, $type, $text, $tags, $source_name, $source_link, $i_bold, $i_photo, $i_video, $off_news, $id));

    $last_id = $id;

    if(isset($_FILES['img']) && $_FILES['img']['size'] > 10) {

        $dir = new Files(H.'/sys/tmp/');
        $dir->setAllowedType(array('jpeg','jpg','png','gif'));

        if($dir->typeChecking($_FILES ['img']['name'])) {
            $typef = $dir->typeFile($_FILES ['img']['name']);
            $namef = 'post_' . $last_id . '.' . $typef;

            if(!$rtr = $dir->upload(array($_FILES['img']['tmp_name'] => $namef))) {
                $doc->err('При загрузки файла ошибка');
                $err = true;
            }

            $folder = substr($last_id, -1);

            $scr = new ImageResize(H.'/sys/tmp/' . $namef);
            $scr->resizeToWidth(492);
            $scr->saveImage(H."/sys/files/news/original/folder".$folder."/".'post_' . $last_id.".jpg" , 90);

            $scr = new ImageResize(H."/sys/files/news/original/folder".$folder."/".'post_' . $last_id.".jpg");
            $scr->resizeToWidth(246);
            $scr->saveImage(H."/sys/files/news/medium/folder".$folder."/".'post_' . $last_id.".jpg" , 90);

            $scr = new ImageResize(H."/sys/files/news/medium/folder".$folder."/".'post_' . $last_id.".jpg");
            $scr->resizeToWidth(160);
            $scr->saveImage(H."/sys/files/news/mini/folder".$folder."/".'post_' . $last_id.".jpg" , 90);



            $img = "folder".$folder."/".'post_' . $last_id.".jpg";

            unlink(H.'/sys/tmp/' . $namef);

            if(!isset($err)) {
                $res = Db::me()->query("UPDATE `news` SET `img` = '$img' WHERE `id` = $last_id");
            }
        } else {
            $err = true;
            $doc->err('Файл не является изоброжением или тип файла не доступен');
        }
    }

    $doc->msg("Новость успешно отредактирована");
}


$form = new Form();

$options = array();
$options[] = array(0, 'Главные новости', $news['type'] == 0);
$options[] = array(1, 'Лента новостей', $news['type'] == 1);
$form->select("type", "Тип новости (Главная, Лента)", $options);

$form->input("title", "Заголовок", $news['title']);

$form->textarea("text", "Содержание новости", $news['text']);

$form->input("source_name", "Название Источник", $news['source_name']);
$form->input("source_link", "Ссылка Источник", $news['source_link']);

$form->input("tags", "Теги (Футбол,Трансферы,Франция)", $news['tags']);

$form->file('img', "Постер min(680x408px)");

$options = array();
$options[] = array(0, 'Не выделять', $news['i_bold'] == 0);
$options[] = array(1, 'Да выделить', $news['i_bold'] == 1);
$form->select("i_bold", "Выделить жирным (-2 балла)", $options);

$options = array();
$options[] = array(0, 'Нет фотографий', $news['i_photo'] == 0);
$options[] = array(1, 'Да есть фотографии', $news['i_photo'] == 1);
$form->select("i_photo", "Присутствует Фото (-2 балла)", $options);

$options = array();
$options[] = array(0, 'Нет видео', $news['i_video'] == 0);
$options[] = array(1, 'Да есть видео', $news['i_video'] == 1);
$form->select("i_video", "Присутствует Видео (-2 балла)", $options);

$form->checkbox("off_news", "Официальная новость", $news['off_news'] != 0);

$form->button("Редактировать", "save");
$form->display();