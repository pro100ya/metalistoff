<?php
include_once 'sys/inc/start.php';
$doc = new Document();


$menu_json = Json::decode(file_get_contents(H . "/sys/json/matches.json"));


$res = Db::me()->query("SELECT * FROM `news` ORDER BY `id` DESC LIMIT 3");

$main_news = array();

foreach($res->fetchAll() AS $key => $news) {
    $main_news[] = array(
        "title" => Text::substr($news['title'], 77),
        "url" => "/news/" . Text::urlToString($news['id'] . "-" . $news['title']),
        //"img" => "/sys/files/news/original/" . $news['img'],
        "text" => Text::substr($news['text'], 190),
        "time" => Misc::whenTime($news['time'])
    );
}

$doc->assign('main_news', $main_news);
$doc->display("news.indexMain.php");


$res = Db::me()->query("SELECT * FROM `news` WHERE `isvideo` = 1 ORDER BY `id` DESC LIMIT 6");

$video_news = array();

foreach($res->fetchAll() AS $key => $news) {
    $video_news[] = array(
        "title" => Text::substr($news['title'], 77),
        "url" => "/news/" . Text::urlToString($news['id'] . "-" . $news['title']),
        //"img" => "/sys/files/news/original/" . $news['img'],
        "text" => Text::substr($news['text'], 190),
        "time" => Misc::whenTime($news['time'])
    );
}

$doc->assign('video_news', $video_news);
$doc->display("news.indexVideo.php");