<?php
include_once 'sys/inc/start.php';
$doc = new Document(1);
$doc->title = 'Настройки';

if(isset($_POST['display'])) {
    $comm_ani = (isset($_POST['dis_ani_comm']) ? 1 : 0);
    $screen_ani = (isset($_POST['dis_ani_screen']) ? 1 : 0);
    
    $user->dis_ani_comm = $comm_ani;
    $user->dis_ani_screen = $screen_ani;
    
    $doc->modalMessages("Изменения приняты");
}

if(isset($_POST['wall'])) {
    $comm_wall = (isset($_POST['comm_wall']) ? 1 : 0);
    $no_wall = (isset($_POST['no_wall']) ? 1 : 0);
    
    $user->comm_wall = $comm_wall;
    $user->no_wall = $no_wall;
    
    $doc->modalMessages("Изменения приняты");
}

if(isset($_POST['newpass'])) {
    if(Crypt::hash($_POST['old_password']) !== $user->password) {
        $doc->modalMessages("Неверный пароль", true);
    } elseif($_POST['password'] !== $_POST['password1']) {
        $doc->modalMessages("пароли не совпали", true);
    } else {
        $user->password = Crypt::hash($_POST['password']);
        $doc->modalMessages("Пароль успешно изменен, незабудте его");
    }
}

if(isset($_POST['privacy'])) {
    $stop_mascot = (isset($_POST['stop_mascot']) ? 1 : 0);
    
    $user->stop_mascot = $stop_mascot;
    
    $doc->modalMessages("Изменения приняты");
}

$form = new Form();
$form->html('<div style="font-size: 14px; margin-top: 3px;">Отображения</div>'
        . '<div style="margin-bottom: 8px;" class="myhr"></div>');
$form->checkbox('dis_ani_comm', 'Не отображать комментарии в аниме', $user->dis_ani_comm);
$form->html('<div style="margin: 8px;"></div>');
$form->checkbox('dis_ani_screen', 'Не отображать скриншоты к аниме', $user->dis_ani_screen);
$form->button("Сохранить", "display");

$form1 = new Form();
$form1->html('<div style="font-size: 14px; margin-top: 10px;">Настройки стены</div>'
        . '<div style="margin-bottom: 8px;" class="myhr"></div>');
$form1->checkbox('comm_wall', 'Запретить писать на стене', $user->comm_wall);
$form1->html('<div style="margin: 8px;"></div>');
$form1->checkbox('no_wall', 'Неотображать стену', $user->no_wall);
$form1->button("Сохранить", "wall");

$form2 = new Form();
$form2->html('<div style="font-size: 14px; margin-top: 10px;">Изменить пароль</div>'
        . '<div style="margin-bottom: 8px;" class="myhr"></div>');
$form2->input("old_password", "Ваш старый пароль");
$form2->password("password", "Ваш новый пароль");
$form2->password("password1", "повторите пароль");
$form2->button("Изменить", "newpass");

$form3 = new Form();
$form3->html('<div style="font-size: 14px; margin-top: 10px;">Приватность</div>'
        . '<div style="margin-bottom: 8px;" class="myhr"></div>');
$form3->checkbox('stop_mascot', 'Запретить дарить талисманы', $user->stop_mascot);
$form3->button("Изменить", "privacy");

echo '<div style="width: 500px;margin: 0 auto;">';
$form->display();
$form1->display();
$form2->display();
$form3->display();
echo '</div>';