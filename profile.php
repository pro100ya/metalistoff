<?php
include_once 'sys/inc/start.php';
$doc = new Document();

$doc->hide_left_vol = true;


$ank = (!isset($_GET ['id'])) ? $user : new User((int)$_GET ['id']);

if(!$ank->group) {
    $doc->accessDenied("Нет пользователя");
}


$doc->title = ($user->id && $ank->id == $user->id)? 'Моя анкета' : 'Анкета ' . $ank->nick;



$doc->addStyle('/sys/themes/default_full/res/pagecss/profile.css?1');




$other = array(
    "edit_avatar" => ($ank->id == $user->id)
);




$top = array();
$top['nick'] = $ank->nick;
$top['names'] = $ank->first_name . ' ' . $ank->last_name;
$top['online'] = ($ank->online) ? 'online' : 'Заходил' . ($ank->sex ? ' ' : 'а ') . Misc::when($ank->last_visit);




$info = array();
$info[] = array('Город', $ank->city);

if ($ank->ank_d_r && $ank->ank_m_r && $ank->ank_g_r) {
    $info[] = array('День рождения', $ank->ank_d_r . ' ' . Misc::getLocaleMonth($ank->ank_m_r) . ' ' . $ank->ank_g_r);
    $info[] = array('Возраст', Misc::getAge($ank->ank_g_r, $ank->ank_m_r, $ank->ank_d_r, true));
} elseif ($ank->ank_d_r && $ank->ank_m_r) {
    $info[] = array('День рождения', $ank->ank_d_r . ' ' . Misc::getLocaleMonth($ank->ank_m_r));
} else {
    $info[] = array('День рождения', 'Незаполнено');
}

$info[] = array('Языки', $ank->languages);
$info[] = array('Интересы', '', true);
$info[] = array('Любимые Книги', $ank->i_books);
$info[] = array('Любимые Фильмы', $ank->i_films);
$info[] = array('Любимые телешоу', $ank->i_teleshow);
$info[] = array('Любимые игры', $ank->i_games);
$info[] = array('Любимая музыка', $ank->i_music);
$info[] = array('Любимые футбольные команды', $ank->i_commands);
$info[] = array('О себе', $ank->about_me);








if(isset($_POST['save']) && $user->group) {
    $text = Text::substr($_POST['message'], 2000);
    $text = Text::inputText($text);

    if($text) {
        $res = Db::me()->prepare("INSERT INTO `wall` (`id_user`, `id_to`, `text`, `time`) VALUES (?, ?, ?, ?)");
        $res->execute(Array($user->id, $ank->id, $text, TIME));

        $user->balls++;

        $doc->msg('Сообщение отправлено');
    }
}



$wall = '';

if($user->id != false) {
    $form = new FormUserComment();
    $form->setUser($user);
    $wall .= $form->fetch();
} else {
    $wall .= '';
}




$pages = new Pages();
$pages->st = 3;
$res = Db::me()->query("SELECT COUNT(*) FROM `wall` WHERE `id_to` = $ank->id");
$pages->posts = $res->fetchColumn(); // количество сообщений

$res = Db::me()->query("SELECT * FROM `wall` WHERE `id_to` = $ank->id ORDER BY `id` DESC LIMIT " . $pages->limit);

$listing = new Listing();

foreach($res->fetchAll() as $comm) {
    $ank_u = new User($comm['id_user']);

    $post = $listing->post();
    $post->id = 'wall' . $comm['id'];
    $post->title = $ank_u->nick;
    $post->url = "/id" . $ank_u->id;
    $post->text = Text::toOutput($comm['text']);
    $post->time = Misc::when($comm['time']);
    $post->img = $ank_u->getAvatar();

    $post->action('<a href="?reply='.$comm['id'].'">Ответить</a>');
    $post->action('<a href="post.edit.php?id='.$comm['id'].'">Изменить</a>');
    $post->action('<a href="post.delete.php?id='.$comm['id'].'">Удалить</a>');
}

$wall .= $listing->fetch('Пока что нет не одного сообщения', 'textView.php');

$wall .= $pages->fetch('?'); // вывод страниц







$doc->assign('ank', $ank);
$doc->assign('other', $other);
$doc->assign('top', $top);
$doc->assign('img', $ank->getAvatar(200, true));
$doc->assign('info', $info);
$doc->assign('wall', $wall);




$doc->display('profile.php');