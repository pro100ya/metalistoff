<?php
include_once 'sys/inc/start.php';
$doc = new Document(1);
$doc->title = 'Выход';

if (isset($_POST['exit'])){
    $res = Db::me()->prepare("DELETE FROM `users_online` WHERE `id_user` = ?;");
    $res->execute(Array($user->id));
    
    //$user->guest_init();
    
    setcookie(COOKIE_ID_USER);
    setcookie(COOKIE_USER_PASSWORD);
    unset($_SESSION);
    session_destroy();

    /* Инициализация механизма сессий  */
    session_name(SESSION_NAME) or die('Невозможно инициализировать сессии');
    @session_start() or die('Невозможно инициализировать сессии');
    header("Location: /index.php");
    exit();
}

$form = new Form('?');
$form->button("Выйти", 'exit');
$form->display();
// ffdd

